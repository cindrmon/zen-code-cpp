#include<gtk/gtk.h>

int main(int argc, char* argv[]){

    // initializes GTK
    gtk_init(&argc, &argv);

    // variable declaration 
    GtkWidget *window, *table, *label, *button;

    // creates a window
    window=gtk_window_new(GTK_WINDOW_TOPLEVEL);

    // X button formation
    g_signal_connect(window,"delete-event", G_CALLBACK(gtk_main_quit),NULL);

    //insert code here
        /* 
        
        gtk_table_attach(GTK_TABLE(table), <object>, 
                            startPoint,finishPointToRight, startPoint,finishPointDownwards,
                            bool_GTKFill, bool_GTKExpand  )
        
        */
        
        // initialize table
        table=gtk_table_new(2, 2, false);

        // button 1?
        button=gtk_button_new_with_mnemonic("_Button");
        label=gtk_label_new("sample Text");

        gtk_table_attach(GTK_TABLE(table),label, 0,1, 0,1, GTK_FILL,GTK_FILL, 4,4 );
        gtk_table_attach(GTK_TABLE(table),button, 1,2, 0,1, GTK_FILL,GTK_FILL, 4,4 );
        
        // button 2?
        button=gtk_button_new_with_mnemonic("_Second Button");
        label=gtk_label_new("sample Text 2");
        gtk_table_attach(GTK_TABLE(table),label, 0,1, 1,2, GTK_FILL,GTK_FILL, 4,4 );
        gtk_table_attach(GTK_TABLE(table),button, 1,2, 1,2, GTK_FILL,GTK_FILL, 4,4 );

    gtk_container_add(GTK_CONTAINER(window),table);

    // shows all widgets
    gtk_widget_show_all(window);

    // starts main loop
    gtk_main();

}