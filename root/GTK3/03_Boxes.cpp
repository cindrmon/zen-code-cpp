#include<gtk/gtk.h>

static void ButtonClicked(GtkWidget* widget, gpointer Data){

    gtk_label_set_text(GTK_LABEL(Data), "Sample Text");

}

int main(int argc, char* argv[]){

    // initializes GTK
    gtk_init(&argc, &argv);

    // variable declaration 
    GtkWidget *window, *label, *button, *hbox;

    // creates a window
    window=gtk_window_new(GTK_WINDOW_TOPLEVEL);

    // X button formation
    g_signal_connect(window,"delete-event", G_CALLBACK(gtk_main_quit),NULL);

    //insert code here

        // initialize label and button
        label=gtk_label_new("New Label");
        button=gtk_button_new_with_label("New Button");

        // callback function
        g_signal_connect(button, "clicked", G_CALLBACK(ButtonClicked),label);

        // initiaalize hbox
        hbox=gtk_box_new(GTK_ORIENTATION_VERTICAL,10);

        gtk_box_pack_start(GTK_BOX(hbox), label, true, false, 10);
        gtk_box_pack_start(GTK_BOX(hbox), button, true, false, 10);

        
    gtk_container_add(GTK_CONTAINER(window),hbox);

    // shows all widgets
    gtk_widget_show_all(window);

    // starts main loop
    gtk_main();

}