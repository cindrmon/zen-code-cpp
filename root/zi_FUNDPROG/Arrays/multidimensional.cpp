#include <iostream>
#define ARRAY_ROW_SIZE 3
#define ARRAY_COL_SIZE 3

using namespace std;

int main() {

    int row_i, col_i;
    int matrix[ARRAY_ROW_SIZE][ARRAY_COL_SIZE] = {{0}};
    int ctr=0;

    for(row_i=0; row_i<ARRAY_ROW_SIZE; ++row_i) {
        for(col_i=0; col_i<ARRAY_COL_SIZE; ++col_i) {

            cout << "Enter number " << col_i << " of " << row_i << ": ";
            cin >> matrix[row_i][col_i];

        };
    }

    for (row_i = 0; row_i < ARRAY_ROW_SIZE; ++row_i) {
        for (col_i = 0; col_i < ARRAY_COL_SIZE; ++col_i) {

            if((matrix[row_i][col_i]%2) == 0)
                ctr+=1;

        };
    }

    cout << "The number of even numbers in the array are " << ctr;

    system("pause");

}