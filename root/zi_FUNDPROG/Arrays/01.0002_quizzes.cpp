#include <iostream>

using namespace std;

int main() {
	
	int quizNo;
	float quiz[quizNo];
	double sum, average;
	
	cout << "Enter the number of quizzes you wish to input: ";
	cin >> quizNo;
	cout << endl << endl;
	
	sum = 0;
	
	for(int x=0; x<quizNo; ++x) {
		
		cout << "Enter quiz number " << x+1 << ": ";
		cin >> quiz[x];
		
		sum = sum + quiz[x];
		
	}
	
	average = sum / quizNo;
	
	cout << "The average is " << average << ".";

}
