#include <iostream>

using namespace std;

void countOddEven(int num[10]) {

		int counter, sumEven=0, sumOdd = 0 ;
		
		for(counter=0; counter<10; ++counter) {

		if( (num[counter]%2) == 0 ) 
			++sumEven;

		}

		for(counter=0; counter<10; ++counter) {

		if( (num[counter]%2) == 1 ) 
			++sumOdd;

		}

	cout << "The number of even numbers are " << sumEven << ".";
	cout << endl << "The number of odd numbers are " << sumOdd << ".";
	cout << endl << endl << endl;

}

/*
int errorCheck(int num, int ctrValue) {

	if(num<=0) {

		cout << "Invalid number!" << endl;
		cout << "Enter value " << ctrValue << ": ";
		cin >> num;

	}

	return num;
}
*/

int main() {

	int numb[10];
	int counter;
	
	counter = 0;

	while(counter<10) {


		cout << "Enter value " << counter+1 << ": ";
		cin >> numb[counter];

		
		if(numb[counter]<=0) {

			cout << "invalid number!" << endl << endl;
			cout << "Enter value " << counter+1 << ": ";
			cin >> numb[counter];

		}//error checking
		

		//numb[counter] = errorCheck(numb[counter], counter);

		else //increments if the error has been resolved
			++counter;

	}//loops for array numb

	countOddEven(numb);
	
	system("pause");
	
}
