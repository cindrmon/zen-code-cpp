//0. INTRODUCTION TO 2 DIMENSIONAL ARRAY/MATRIX

    /*

        **FORMAT FOR 2-DIMENSIONAL ARRAY/MATRIX**

        for(int i=0; i<rowValue; ++i) {                  -->loop for each row to be selected

            // row separators optional, for output use (often endl is used)
        
            for(int j=0; j<columnValue; ++j) {          -->loop to select which column in row i

                // insert arguments here for loop
                // columnar separators optional, for output use (often spaces or tabs are used)

            }

            // other row separators

        }

        output should be like this:

               j    j+1   j+2    j+n
        i      x     y     z      n
        i+1    a     b     c      n
        i+2    d     e     f      n
        i+n    ng    nh    ni     n

    extra notes when making matrix:
        1. always #define MAX_COL and MAX_ROW (or edit it here ig)
        2. row first before column

    */

//0.5. Current Functions in this header file:
    /*
        input/output of matrix:
            1. inputMatrix(Matrix, Rows, Columns)
            2. printMatrix(Matrix, Rows, Columns)
        other functions:
            1. equalityMatrix(matrix1, matrix2, rows, columns) 
                    --> Tests out if the two matrices are equal or not
    */

   using namespace std;


//1. Default Values
    //default values of max size of matrix
    #define MAX_COL 100
    #define MAX_ROW 100

//2. input/output usage functions in accessing matrix

    //input function for matrix
    void inputMatrix(int Matrix[][MAX_COL], int Row, int Col) {

        int i, j;  //indexes

        for(i=0; i<Row; ++i) {

                cout << "                          [ r, c ]";
            cout << endl;

            for(j=0; j<Col; ++j) {

                cout << "Enter data at coordinates [ " << i+1 << ", " << j+1 << "] : ";
                cin >> Matrix[i][j];
            }

            cout << endl;
        }

    }

    //output function for matrix
    void printMatrix(int Matrix[][MAX_COL], int Row, int Col) {

        cout << "DATA INSIDE THIS MATRIX";
        cout << endl << "==============================================";
        cout << endl;


        for(int i=0; i<Row; ++i){
            for(int j=0; j<Col; ++j) {

                cout << "  " << Matrix[i][j] << "\t"; 

            } //prints what's inside row i

            cout << endl;

        }

        cout << "==============================================" << endl;
    }

//3. other purposes for martrices

    //compares two matrices and tests if they are inequal or not
    int equalityMatrix(int Matrix1[][MAX_COL], int Matrix2[][MAX_COL], int Row, int Col) {

        for (int i = 0; i < Row; ++i){
            for (int j = 0; j < Col; ++j){

                if (Matrix1[i][j] != Matrix2[i][j])
                    return 0;
            }
        }

        return 1;
    }