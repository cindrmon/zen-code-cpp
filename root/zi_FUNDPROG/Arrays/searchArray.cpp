#include <iostream>

using namespace std;

int searchArray(const int list[], int listLength, int searchItem) {

    int index;

    index = 0;
    while(index<listLength) {

        if(list[index] == searchItem)
            return(index);
        else
            ++index;
        
    
    }
    return(-1);
}

int main() {

    int list[10], listLength, searchItem;
    int x;
    int searchResult;

    cout << "Enter the length of array: ";
    cin >> listLength;

    for(x=0; x<listLength; ++x) {

        cout << "Enter value " << x+1 << ": ";
        cin >> list[x];

    }

    cout << "What value do you wish to search for in the friggin array? " << endl;
    cin >> searchItem;

    searchResult = searchArray(list, listLength, searchItem);

    if(searchResult == -1)
        cout << "Can't find item";
    else
        cout << "The value is in list[" << searchResult << "].";

return 69;

}