#include <iostream>
using namespace std;

main()
{
    const int AS = 10;
    int array1[AS] = {5, 10, 15, 20, 25, 30, 35, 40, 45, 50};
    int array2[AS]; //will contain the reverse of array1
    //int temp, x, y = (AS-1);

    cout << "\nCONTENST OF ARRAY 1";
    for (int i = 0; i < AS; ++i)
    {
        cout << "\n"
             << array1[i];
    }

    /*YOUR CODE GOES HERE THAT WILL REVERSE THE CONTENTS OF ARRAY1 INTO ARRAY2
    for(x=0; x<AS; ++x) { //2 vars

        temp = array1[y];
        array2[x] = temp;

        --y;

    }

    */

   for(int i=0; i<AS ; ++i) { //no vars

       array2[i] = array1[(AS-1)-i];

   }

    cout << "\nCONTENST OF ARRAY 2";
    for (int i = 0; i < AS; ++i)
    {
        cout << "\n"
             << array2[i];
    }

    cout << endl;
    system("pause");
}