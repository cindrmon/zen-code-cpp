#define MAX_COL 2
#define MAX_ROW 12

using namespace std;

/*
        Highest     Lowest
    Jan
    Feb
    Mar
    ...
    Dec
    
    
    
    */

void getData(double tempArray[][MAX_COL]) {

    char Month[12][5]={"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

    for (int i = 0; i<MAX_ROW; ++i) {
        for (int j = 0; j<2; ++j){

            if(0==j) {

                    cout << "Enter the highest temperature of month " << i+1 << ": ";
                    cin >> tempArray[i][j];

            }
            else {

                cout << "Enter the lowest temperature of month " << i + 1 << ": ";
                cin >> tempArray[i][j];
                
            }

        cout << endl;

        }
    }

    cout << endl << endl;
    cout << "DATA GATHERED" << endl;
    cout << "============================================" << endl;
    cout << "Month No. \t Highest Temp \t Lowest Temp" << endl;

        for (int i=0; i<MAX_ROW; ++i){

            cout << Month[i] << "\t\t ";

            for(int j=0; j<2; ++j){

                cout << tempArray[i][j] << "\t\t ";

            }

            cout << endl;
        }

    cout << "=============================================";

}

int indexofHighTemperature(double tempArray[][2]) {

    double topmostValue = 0.00;

    for (int i = 0; i < MAX_ROW; ++i){

        if(tempArray[i][0]>topmostValue)
            topmostValue = tempArray[i][0];        

    }

    for(int x=0; x<MAX_ROW; ++x) {

        if(topmostValue==tempArray[x][0])
            return(x);

    }

}

int indexofLowTemperature(double tempArray[][2]) {

    double bottommostValue = 0.00;

    for (int i = 0; i < MAX_ROW; ++i){

        if (tempArray[i][1]<tempArray[i+1][1])
        bottommostValue = tempArray[i][1];

    }

    for (int x = 0; x < MAX_ROW; ++x)
    {

        if (bottommostValue == tempArray[x][1])
            return (x);
    }
}

double averageHigh(double tempArray[][2]) {

    double sumHigh = 0, sumAverageHigh = 0;

    for (int i = 0; i < MAX_ROW; ++i) {
        for (int j = 0; j < 2; ++j){

            if(j==0) {

                sumHigh += tempArray[i][0];

            }

        }
    }

    sumAverageHigh = sumHigh / 12;

    return(sumAverageHigh);

}

double averageLow(double tempArray[][2]) {

    double sumLow = 0, sumAverageLow = 0;

    for (int i = 0; i < MAX_ROW; ++i){
        for (int j = 0; j < 2; ++j){

            if(j==1) 
                sumLow += tempArray[i][1];

        }
    }

    sumAverageLow = sumLow / 12;

    return(sumAverageLow);

}
