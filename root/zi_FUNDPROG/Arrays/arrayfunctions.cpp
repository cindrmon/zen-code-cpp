#include <iostream>

using namespace std;

void timesTwo(int numbers[5]) {

	int count;

	for(count=0; count<5; ++count) 
		numbers[count] *= 2;

}

int main() {

	int numbers[5] = {5, 10, 15,20,25};

	int index;

	cout << endl << "The contents of the array are: ";

	for(index=0; index<5; ++index) 
		cout << numbers[index] << " ";

	timesTwo(numbers);

	cout << endl << "The contents of the array are: ";

	for(index=0; index<5; ++index)
		cout << numbers[index] << " ";


}
