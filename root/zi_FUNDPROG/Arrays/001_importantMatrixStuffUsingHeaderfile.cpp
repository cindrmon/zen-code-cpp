#include <iostream>
#include "matrix_cpp.h"

using namespace std;

int main() {

    int matrix1[MAX_ROW][MAX_COL];
    int matrix2[MAX_ROW][MAX_COL];
    int actualRow, actualColumn;
    bool isEqual;

    cout << "Enter number of rows: ";
    cin >> actualRow;

    cout << "Enter number of columns: ";
    cin >> actualColumn;

    cout << endl << endl;
    cout << "Currently inputting Matrix1: " << endl << endl;
    inputMatrix(matrix1, actualRow, actualColumn);
    printMatrix(matrix1, actualRow, actualColumn);

    cout << endl << endl;
    cout << "Currently inputting Matrix2: " << endl << endl;
    inputMatrix(matrix2, actualRow, actualColumn);
    printMatrix(matrix2, actualRow, actualColumn);

    isEqual = equalityMatrix(matrix1, matrix2, actualRow, actualColumn);

    if(isEqual) 
        cout << endl << endl << "The matrices are equal.";
    else
        cout << endl << endl << "The matrices are unequal";

    cout << endl << endl;
    system("pause");
    

}

