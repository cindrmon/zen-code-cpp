#include <iostream>

//default max values for matrix
#define MAX_ROW 100
#define MAX_COL 100

using namespace std;

/*

    **FORMAT FOR 2-DIMENSIONAL ARRAY/MATRIX**

    for(int i=0; i<rowValue; ++i) {                  -->loop for each row to be selected
        for(int j=0; j<columnValue; ++j) {          -->loop to select which column in row i

            // insert arguments here for loop
            //columnar separators optional, for output use (often spaces or tabs are used)

        }

        //row separators optional, for output use (often endl is used)
    }
*/


//input function for matrix
void inputMatrix(int Matrix[][MAX_COL], int Row, int Col) {

    int i, j;  //indexes

    for(i=0; i<Row; ++i) {

            cout << "                          [ x, y ]";
        cout << endl;

        for(j=0; j<Col; ++j) {

            cout << "Enter data at coordinates [ " << i+1 << ", " << j+1 << "] : ";
            cin >> Matrix[i][j];
        }

        cout << endl;
    }

}

//output function for matrix
void printMatrix(int Matrix[][MAX_COL], int Row, int Col) {

    cout << "DATA INSIDE THIS MATRIX";
    cout << endl << "==============================================";
    cout << endl;


    for(int i=0; i<Row; ++i){
        for(int j=0; j<Col; ++j) {

            cout << "  " << Matrix[i][j] << "\t"; 

        } //prints what's inside row i

        cout << endl;

    }

    cout << "==============================================" << endl;
}


//compares two matrices and tests if they are inequal or not
int equalityMatrix(int Matrix1[][MAX_COL], int Matrix2[][MAX_COL], int Row, int Col) {

    for (int i = 0; i < Row; ++i) {
        for (int j = 0; j < Col; ++j) {

            if(Matrix1[i][j] != Matrix2[i][j]) 
                return 0;

        }

    }

    return 1;
}

int main() {

    int matrix1[MAX_ROW][MAX_COL];
    int matrix2[MAX_ROW][MAX_COL];
    int actualRow, actualColumn;
    bool isEqual;

    cout << "Enter number of rows: ";
    cin >> actualRow;

    cout << "Enter number of columns: ";
    cin >> actualColumn;

    inputMatrix(matrix1, actualRow, actualColumn);
    printMatrix(matrix1, actualRow, actualColumn);

    inputMatrix(matrix2, actualRow, actualColumn);
    printMatrix(matrix2, actualRow, actualColumn);

    isEqual = equalityMatrix(matrix1, matrix2, actualRow, actualColumn);

    if(isEqual) 
        cout << endl << endl << "The matrices are equal.";
    else
        cout << endl << endl << "The matrices are unequal";

    cout << endl << endl;
    system("pause");
    

}

