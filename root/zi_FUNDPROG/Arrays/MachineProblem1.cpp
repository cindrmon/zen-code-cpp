#include <iostream>
#define ARRAY_SIZE 10

using namespace std;

int main() {

    int array[ARRAY_SIZE];
    int size = ARRAY_SIZE;
    int sum = 0;
    int x;
    char choice;

    do{

        system("cls");
        cout << endl;

        for(x=0; x<size; ++x) {

            cout << "Enter integer " << x+1 << ": ";
            cin >> array[x];
            
        }

        for(x=0; x<size; ++x) {

                if(array[x]>=5 && array[x]<=10)
                sum += array[x];

        }

        cout << "The sum of numbers between 5 and 10 is " << sum << ".";
        cout << endl << endl;
        cout << "Do you want to try again?" << endl;
        cout << "Type Y for Yes, and N for No: ";
        cin >> choice;

    } while(choice == 'y' || choice == 'Y');

    system("cls");

    cout << "Thank you for using this program" << endl;
    cout << "Program Terminating!" << endl;
    
    system("pause");
    system("cls");    

}