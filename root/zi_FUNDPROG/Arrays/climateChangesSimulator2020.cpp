#include <iostream>
#include "climateChangeFunctions.h"

using namespace std;



int main() {

    double temperature[12][2] = {0};
    int indexofHighTempYear, indexofLowTempYear;
    double averageHighTemp, averageLowTemp;

    cout << "GLobal Warming Simulatoer 2020!" << endl << endl;

    getData(temperature);

    indexofHighTempYear = indexofHighTemperature(temperature)+1;
    indexofLowTempYear = indexofLowTemperature(temperature)+1;

    averageHighTemp = averageHigh(temperature);
    averageLowTemp = averageLow(temperature);

    cout << endl << endl;

    cout << "=============================================" << endl
         << "OTHER DATA:" << endl
         << "Average High Temperature: " << averageHighTemp << endl
         << "Average Low Temperature: " << averageLowTemp << endl
         << endl
         << "Highest Temperature this year is on month: " << indexofHighTempYear << endl
         << "Lowest Temperature this year is on month: " << indexofLowTempYear << endl;

    system("pause");

}