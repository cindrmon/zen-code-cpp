#include <iostream>

using namespace std;

int main() {

    int ARRAY_SIZE;
    int integers[100] = {0};
    int maxNum = 0;
    int x;

    x=0;

    //asks for user input on how many numbers (arrays) they want to enter
    cout << "How many numbers do you want to enter? ";
    cin >> ARRAY_SIZE;

     while( (ARRAY_SIZE>100) || (ARRAY_SIZE<=0) ){

        cout << "Invalid Input! (Value must be greater than 0 or less than 100)" << endl;

        cout << "How many numbers do you want to enter? ";
        cin >> ARRAY_SIZE;

    }//error checking

    for(x=0; x<ARRAY_SIZE; ++x){

        cout << "Enter value " << x+1 << ": ";
		cin >> integers[x];

    }//asks user for input

    for(x=0; x<ARRAY_SIZE; ++x) {

        if(maxNum<integers[x])
            maxNum = integers[x];

    }//searches for highest number in the group

    cout << "The highest number is " << maxNum << "." << endl;
    cout << "The indices are: ";

    for(x=0; x<ARRAY_SIZE; ++x) {

        if(integers[x] == maxNum)
            cout << x << " ";

    }//searches for where the highest number is located

}