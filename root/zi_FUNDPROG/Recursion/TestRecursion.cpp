#include <iostream>

using namespace std;

int countdown(int num) {

	if(num<1) {
		return num;
	}

	else {
		cout << num << " ";
		countdown(num-1);
		return(num);
	}

}


int countup(int one, int num) {
		
	if(one >= num) {
		cout << num;
		return 0;
	}

	else {
		cout << one << " ";
		countup(one+1, num);
		return(one);
		
	}

}

void countvp (int num) {

	if (num != 1) 
	countvp (num - 1);
	
	cout << num << " ";

}

int main() {
	
	int one = 1, num;

	cout << "Input number: "; cin >> num;
	
	cout<< "Using the countdown function... :";
	countdown(num);
	
	cout << endl << endl;
	
	cout << "Using the countup function... :";
	countup(one, num);
	
	cout << endl << endl;
	
	cout << "Using the countvp function... :";
	countvp(num);

}
