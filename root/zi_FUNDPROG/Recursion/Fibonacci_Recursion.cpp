#include <iostream>

using namespace std;

int fibonacci(int n) {
	
	if(n<2)
		return 1;

	else
		return fibonacci(n-1) + fibonacci(n-2);

}

void introMessage() {

	cout << endl << "=================================================";
	cout << endl << "===                                           ===";
	cout << endl << "===    Welcome to the Fibonacci Program!!!    ===";
	cout << endl << "===  This program tells the fibonacci number  ===";
	cout << endl << "===             of your choice!!!             ===";
	cout << endl << "===                                           ===";
	cout << endl << "=================================================";

}

void invalidMessage() {
	
	system("cls");
	cout << endl << "=================================================";
	cout << endl << "===                                           ===";
	cout << endl << "===              Invalid Input!!!             ===";
	cout << endl << "===                                           ===";
	cout << endl << "===   Value input must be a negative number.  ===";
	cout << endl << "===             Please try again!             ===";
	cout << endl << "===                                           ===";
	cout << endl << "=================================================";
	
}

void inputMessage() {
	
	cout << endl << endl << "Enter a positive integer: ";
	
}

int inputLoop() {
	
	int n;

	introMessage();
	inputMessage(); cin >> n;
	
	while (n<0) {
		
		invalidMessage();
		inputMessage(); cin >> n;
				
	} 	
		return n;
}

char fibonacciMessage(int ans) {
	
	char choice;
	
	system("cls");
	cout << endl << "=================================================";
	cout << endl << "===                                           ===";
	cout << endl << "===         The fibonacci number is " << fibonacci(ans) << ".        ===";
	cout << endl << "===                                           ===";
	cout << endl << "===      Do you wish to try again? (Y/N)      ===";
	cout << endl << "===            (Not case sensitive)           ===";
	cout << endl << "===                                           ===";
	cout << endl << "=================================================";
	cout << endl << ">> "; cin >> choice;
	
	return choice;	
	
}

void exitMessage() {
	
	cout << endl << "=================================================";
	cout << endl << "===                                           ===";
	cout << endl << "=== Thank you for using this program! Kanpai! ===";
	cout << endl << "===                                           ===";
	cout << endl << "=================================================";	
	
}

int main() {

	char choice;

	do {
		
		system("cls");
		choice = fibonacciMessage(inputLoop());
		
	} while(choice == 'Y' || choice == 'y');
	
	system("cls");
	exitMessage();

}
