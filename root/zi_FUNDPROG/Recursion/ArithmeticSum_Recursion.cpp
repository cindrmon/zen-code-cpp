#include <iostream>

using namespace std;



int accumulation(int number) {
	
	int ans; //puts the value of the sum here
	
	if(number == 1)
		return 1; //base for accumulation function recursion to
				  //add each value decremented
		
	else
		ans = accumulation(number - 1) + number;
		return ans;//returns the sum

}


int main() {
	
	int number, sum;
	
	cout << "Enter the value of n: "; cin >> number;
	
	sum = accumulation(number);
	
	cout << "The sum is " << sum;

}
