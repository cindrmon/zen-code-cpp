/* different types of character processing functions:

        toupper()
        tolower()
        isupper()
        islower()
        isalpha()
        isdigit()
        isspace()
        isalnum()
        ispunct()

    cin.get(varChar, lengthOfCharacterArray) --> used for character input

    strins is an array of characters

    for(int i=0; stringName[i]!='0'; ++i){

        insert arguments here

    }

    STRING FUNCTIONS: <-------------------------------------------------    flow

        strcpy() --> copies string/variable to variable
            strcpy(variable, "String");
        
        strcmp() --> compares variable1 to variable2. if both are equal, value is 0.
            strcmp(variable1, variable2);

        strlen() --> string length excluding NUL character
            strlen(variable/"string");

        strcat() --> concatenates strings from two variables/strings of the variable
            strcat(variable/"string1", variable2/"string2" );


*/