#include <iostream>
#include <cmath>

//Constants to be used
#define IN 12
#define M 0.0254
#define KG 2.2

using namespace std;

//Functions used
int ftInToInchesOnly(int feet, int inches) {
	
	int inchFromFeet, feetInches;
	
	inchFromFeet = feet * IN;
	feetInches = inchFromFeet + inches;
	
	return feetInches;
	
} //converts feet inches to inches only

double inchesToMetres(int inches) {
	
	return inches * M;
	
} //converts inches to metres

double poundsToKilos(double pounds) {
	
	return pounds / KG;
	
} //converts pounds to kilogrammes

void BMIResults(double BMI) {
	
	if(BMI < 18.5) {
		
		cout << "Ur underweight. Magpataba ka";
		
	}
	
	else if(BMI >= 18.5 && BMI < 25.0) {
		
		cout << "Ur healthy. Keep up the good work!";
		
	} 
	
	else if (BMI >= 25.0 && BMI < 30.0) {
	
		cout << "Ur overweight, u can be thin still.";
	
	}
	
	else if (BMI >= 30 && BMI < 35) {
		
		cout << "Ur OBESE I na!!!";
		
	}
	
	else if (BMI >= 35 && BMI < 40) {
		
		cout << "UR OBESE II NA!!! FIX UR LIFE!!";
		
	}
	
	else {
		
		cout << "UR OBESE III!!! my god.. what have u done to yourself..";
		
	}
	
} // Checks if your BMI is normal or not, informs you if its not, and gives
  //insights and "motivation" for your current BMI

int main() {
	
	//variables for main 
	int feet, inches;
	double pounds;
	double metres, kilogrammes;
	double BMI;
	
	//input values
	cout << "Test Your BMI Now!!!\n\n";
	cout << "Enter yout height in feet in inches." << endl;
	cout << "First, enter the feet: "; cin >> feet;
	cout << "Next, enter the inches: "; cin >> inches;
	cout << "Finally, enter your weight in pounds: "; cin >> pounds;
	
	//execution of functions	
	inches = ftInToInchesOnly(feet, inches);
	
	metres = inchesToMetres(inches);
	
	kilogrammes = poundsToKilos(pounds);
	
	
	//output of values
	cout << endl << "Your height in meters is " << metres << endl;
	cout << "Your weight in kilograms is " << kilogrammes << endl;
	
	BMI = kilogrammes / pow(metres, 2);
	
	cout << "Your BMI is " << BMI << endl << endl;
	
	BMIResults(BMI); 
	
	
}
