#include <iostream>
// #include <conio.h>

using namespace std;

float sales_tax (float c) {
	
	float x;
	
	x = c + (c * 0.06);
	
	return(x);	
	
}

int main() {
	
	float cost, cash, change;
	
	cout << "Enter the cost of the item: ";
	cin >> cost;
	
	cost = sales_tax(cost);
	
	cout << "The total cost of the item including the sales tax is " << cost << " pesos." << endl;
	
	cout << "Enter the amount tendered: ";
	cin >> cash;
	
	change = cash - cost;
	
	cout << "The amount tendered is " << cash << " pesos." << endl;
	cout << "Therefore, the change is " << change << " pesos." << endl;
	cout << "Press any key to continue.";
	
	//getch();
	
}
