#include <iostream>
#include <conio.h>

using namespace std;

float inflation_rate(float l, float m) {  //function to calculate the inflation rate, approx.
	
	float x;
	
	x = (m - l) / l;
	
	return(x);
	
	
}

float next_years_price(float i, float c) {  //function to calculate next year's price, approx.
	
	float z;
	
	z = c + (i * c);
	
	return(z);
	
}

int main() {
	
	float l, t, inf, ny_p;
	
	
	cout << "Enter the price of the item last year: ";
	cin >> l;
	
	cout << "Enter the price of the item this year: ";
	cin >> t;
	
	inf = inflation_rate(l, t);
	
	cout << "The inflation rate is " << inf * 100 <<"%" << endl;

	ny_p = next_years_price(inf, t);

	cout << "The price of this item next year is " << ny_p << " pesos." << endl;
	cout << "Press any key to continue";	
	
	getch();
}

