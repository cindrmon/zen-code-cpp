#include <iostream>
#include <conio.h>


using namespace std;

//my solution
double pow(double x, double e) {
	
	double output;
	int ctr;
		
	output = 1;
	ctr = 0;
	
	while(ctr < e) {
		
		output *= x;
		++ctr;
		
	}		
	
	return (output);
	
}

int factorial(int input) {

	int factorial, ctr;
	
	factorial = 1;
	ctr = 1;
	
	while(ctr <= input) {
		
		factorial = factorial * ctr;
		++ctr;
		
	}
	
	return(factorial);
	
}


main() {
	
	int choice, exit;
	
	double base, exponent;
	
	int fact_in;
	
	start: {
	
		system("cls");
	
		cout << "This program can calculate either the power of a number\n";
		cout << "or a factorial of a number." << endl;
		cout << "Type 1 for solving exponent \nType 2 for factorial\n";
		cout << "Type 3 to exit program\n\nEnter input here: ";
		cin >> choice; 
		
		switch (choice) {
			
			case 1:
				
				system("cls");
				cout << "You have selected the power of a number!\n";
				cout << "Input the base of the number (_^x): "; cin >> base;
				cout << endl << "Input the exponent/power of the number (n^_): "; cin >> exponent;
				
				cout << endl << endl << base << "^" << exponent << " is " << pow(base, exponent) << ".";
				cout << endl << endl;
				
				break;
				
			case 2:
				
				system("cls");
				cout << "You have selected the factorial of a number!\n";
				cout << "Input the number you wish to solve a factorial with (_!=n): "; cin >> fact_in;
				
				cout << fact_in << "! is " << factorial(fact_in) << ".";
				cout << endl << endl;
				
				break;
				
			case 3:
				goto end;
				break;
				
			default:
				
				cout << "Invalid Input! Pls try again..";
				cout << endl << endl;
				goto start;
		}
	
	}
	
	final: {
			
		cout << "Press any key to continue: ";
		getch();
		system("cls");
		
		cout << "Thanks for using this program! Would you wish to use it again?\n";
		cout << "1 for yes, 2 for no\n\n";
		cout << ">> "; cin >> exit;
		
		switch (exit) {
		
			case 1:
				
				goto start;
				
				break;
				
			case 2:
				
				goto end;
				
				break;
				
			default:
				
				cout << "invalid choice, try again!!\n\n";
				goto final;
				
		}
		
	}
	
	end: {
		
		system("cls");
		cout << "Thanks for using this program!!";
	
	
	}
		
}
