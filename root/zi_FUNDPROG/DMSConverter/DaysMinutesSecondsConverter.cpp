#include <iostream>
#include <conio.h>

using namespace std;

float dms_converter(float d, float m, float s) {
	
	float x, mc, sc;
	
	mc = m / 60;
	sc = s / 3600;
	
	x = d + mc + sc;
	
	return (x);
	
}

int main() {
	
	float deg, mins, secs, dec;
	
	cout << "Enter a latitude in degrees, minutes, and seconds: " << endl;
	
	cout << "First, enter the degrees: ";
	cin >> deg; 
	
	cout << "Next, enter the minutes of arc: ";
	cin >> mins;
	
	cout << "Finally, enter the seconds of arc: ";
	cin >> secs;
	
	dec = dms_converter(deg, mins, secs);
	
	cout << deg << " degrees,";
	cout << mins << " minutes, ";
	cout << secs << " seconds = ";
	cout << dec << " degrees";
	
	getch();
	
}
