#include <iostream>

using namespace std;

main() {

	int num, ctr;
	bool prime;
	
	cout << "Enter an integer: "; cin >> num;
	
	while(num <= 1) {
		
		cout << "Number must be greater than 1!\n";
		cout << "Please try again";
		
		cout << "Enter an integer: "; cin >> num;	
		
	}
	
	prime = true;
	
	for(ctr = 2; ctr<num ; ++ctr ) {
		
		cout << ctr << endl;
		
		if( (num%ctr) == 0 ) {
			
			prime = false;
			break;
		
		}
		
		
	}
	
	if(prime)
		cout << endl << "This is a prime number";
	else
		cout << endl << "This is a composite number";
	
}
