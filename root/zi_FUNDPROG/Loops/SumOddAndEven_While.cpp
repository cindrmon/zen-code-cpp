#include <iostream>

using namespace std;

main() {
	
	
	int sum_even, sum_odd;
	int input;
	
	sum_even = 0;
	sum_odd = 0;
	
	
	cout << "Enter a positive integer: "; cin >> input;
	
	
	while(input >= 0) {
		
		if( ( input%2 ) == 0 )
			sum_even += input;	
		else
			sum_odd += input;
			
		cout << "Enter a positive integer: "; cin >> input;
	
	}
	
	cout << endl << "The sum of all even numbers is " << sum_even;
	cout << endl << "The sum of all odd numbers is: " << sum_odd << ".";
	
}
