#include <iostream>

using namespace std;

main() {
	
	int num, n, sum;
	num = 1;
	sum = 0;
	
	cout << "Enter the value of n: "; cin >> n;
	cout << "The Natural numbers from 1 to " << n << " are: " << endl;
	
	while(num<=n) {
		
		cout << num << " ";
		sum = sum + num;
		++num; 
		
	}
	
	cout << endl;
	cout << "The sum of the numbers is: " << sum << ".";
	
	
}
