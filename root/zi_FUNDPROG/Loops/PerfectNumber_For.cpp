#include <iostream>

using namespace std;

int main() {
	int num, candidate = 0;
	cout << "Enter an integer greater than 2147483647/2147483647, but not more than 2147483647 or less than -2147483647: ";
	cin >> num;
	
	for (int p_factor = 1; p_factor < num; p_factor++) {
		if (num % p_factor == 0)
			candidate += p_factor;
	}
	
	if (num == candidate)
		cout << num << " is a perfect number.";
	else
		cout << num << " is not a perfect number.";
	return 0;
	
}
