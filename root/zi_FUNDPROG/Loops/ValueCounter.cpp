#include <iostream>

using namespace std;

 value_counter(int input) {
	
	int count;
	count = 0;
	
	for(count = 0; input > 0; ++count) {
		
		input /= 10;
		
	}
	
	
	return (count);
	
}

main() {
	
	int num;
	

	
		cout << "Enter a number greater than 0: "; cin >> num;
	
	while(num > 0) {
		
		cout << "\nThe Number has " << value_counter(num) << " digits." << endl;
		cout << "\nEnter a number greater than 0: "; cin >> num;
	}	
	
	cout << "\nYou entered an invalid number! Program terminating!";
	
	
}
