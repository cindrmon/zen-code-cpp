#include <iostream>



using namespace std;

//my solution
pow(double x, double e) {
	
	double output;
	int ctr;
	
	output = 1;
	ctr = 0;
	
	while(ctr < e) {
		
		output *= x;
		++ctr;
	}
	
	return(output);
	
}

/*
factorial_function(int x) {

	int output_2;
	int ctr;
	
	output_2 = 1;
	ctr = 1;
	
	while(ctr <= x) {
		
		output_2 *= ctr;
		++ctr;
		
	}

}
*/


main() {
	
	double base, exponent;
	int fac_input, factorial;

	cout << "Enter base: "; cin >> base;
	cout << "Enter exponent: "; cin >> exponent;
	
	cout << "The answer is " << pow(base, exponent) << ".";
	
	cout << endl << endl << "Enter Factorial: "; cin >> fac_input;
	
	
	int ctr;
	
	factorial = 1;
	ctr = 1;
	
	while(ctr <= fac_input) {
		
		factorial = factorial * ctr;
		++ctr;
		
	}
	
	
	cout << fac_input << "! is " << factorial << ".";
	
	
}
