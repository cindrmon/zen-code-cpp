#include <iostream>

using namespace std;

int main() {
	
	int a_population, a_growth_rate;
	int b_population, b_growth_rate;
	int year_count = 0; // initialized for special purposes
	
	cout << "Enter the population for town A: "; cin >> a_population;
	cout << "Enter the G-RATE for town A: "; cin >> a_growth_rate;
	cout << "Enter the population for town B: "; cin >> b_population;
	cout << "Enter the G-RATE for town B: "; cin >> b_growth_rate;
	
	while (a_population < b_population) {
		a_population *= 1 + (0.01 * a_growth_rate);
		b_population *= 1 + (0.01 * b_growth_rate);
		++year_count;
	}
	
	cout << "It will take " << year_count << " years for town A to EVISCERATE town B.";
	
	return 0;
	
}
