#include <iostream>
#include <string>

using namespace std;

double ComputeAverage(double Midterm, double Prefinal) {

	return Midterm*0.5 + Prefinal*0.5;

}

string DetermineRemarks(double Average) {

	string Remarks = "null";

	if(Average>=70) 
		Remarks = "Passed";

	else
		Remarks = "Failed";

	return Remarks;

}

void print(string ID, string Name, double Midterm, double Prefinal, double FinalGrade, string Remarks) {

	system("clear");

	cout 
		<< "ID: " << ID << endl
		<< "Name: " << Name << endl
		<< "Midterm Grade: " << Midterm << endl
		<< "Pre-final Grade: " << Prefinal << endl
		<< endl
		<< "Final Grade: " << FinalGrade << endl
		<< "Remarks: " << Remarks << endl; 

}


int main() {

	string id = "null";
	string name = "null";
	double midterm = 0;
	double preFinal = 0;

	double finalGrade = 0;
	string remarks = "null";

	cout << "Enter Student ID: "; cin >> id;
	cout << "Enter Student Name: "; cin >> name;
	cout << "Enter Midterm Grade: "; cin >> midterm;
	cout << "Enter Pre Final Grade: "; cin >> preFinal;

	finalGrade = ComputeAverage(midterm,preFinal);
	remarks = DetermineRemarks(finalGrade);

	print(id, name, midterm, preFinal, finalGrade, remarks);


}