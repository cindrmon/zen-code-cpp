#include <iostream>
#include <cmath>
#define PI 3.14159265359

typedef double element;

using namespace std;

void areaCircle(element radius, element *area) { 
        *area = PI * pow(radius,2);
}

int main() {

    element radius, area;

    cout << "Enter the radius: ";
    cin >> radius;



    areaCircle(radius, &area);

    cout << endl << "The area of the circle is " << area << endl;



}