#include <iostream>
#include <string>
#include <cstring>
#include <iomanip>

//fixes values to 2 decimal places
#define FIX fixed << setprecision(2)

using namespace std;

class EmployeesPay {
        
    private:
        
        // user input
        string EmployeeID;
        string EmployeeName;
        string SalesCode;
        double SalesAmount;

        // additionals
        const double AdditionalSalesCommission = 0.05;

        // output
        double TakeHomePay;
        double GrossEarnedAmount;
        double SalesCommission;

    public:

        // init
        EmployeesPay() {

            EmployeeID = "000000";
            EmployeeName = "nul";
            SalesCode = "nul";
            SalesAmount = 0;

            TakeHomePay = 0;
            GrossEarnedAmount = 0;
            SalesCommission = 0;
            
        }
        
        // destructor
        ~EmployeesPay() {}

        // Setter/Getter
        
            void setEmployeeID(string EID){
                this->EmployeeID = EID;
            }

            void setEmployeeName(string EName) {
                this->EmployeeName = EName;
            }

            void setSalesCode(string SalesCd) {
                this->SalesCode = SalesCd;
            }

            void setSalesAmount(double SalesAmt) {
                this->SalesAmount = SalesAmt;
            }


            string getEmployeeID() {
                return EmployeeID;
            }

            string getEmployeeName() {
                return EmployeeName;
            }

            string getSalesCode() {
                return SalesCode;
            }

            double getSalesAmount() {
                return SalesAmount;
            }

        // sales codes computation
        void
        SalesCodeAlpha(){

            GrossEarnedAmount = 175 + (0.5 * SalesAmount);
        }

        void SalesCodeBeta () {

            GrossEarnedAmount = 100 + (0.2 * SalesAmount);

        }

        // additional sales commission
        void SalesAdditionalCommission() {

            if(SalesAmount>=2500)
                SalesCommission = 0.05*SalesAmount;

        }

        // compute for take home pay
        void TakeHomePayFinal () {

            SalesAdditionalCommission();

            if( SalesCode=="A" || SalesCode=="a" ) {

                SalesCodeAlpha();
                TakeHomePay = GrossEarnedAmount+SalesCommission;

            }
            else if ( SalesCode=="B" || SalesCode=="b" ) {

                SalesCodeBeta();
                TakeHomePay = GrossEarnedAmount+SalesCommission;

            }
            else {

                cout << "Error!"; // just in case?

            }
        }

        void PrintDetails() {

            TakeHomePayFinal();

            system("clear");

            cout
                << "Employee ID: " + EmployeeID << endl
                << "Employee Name: " + EmployeeName << endl
                << "Sales Code: " + SalesCode << endl
                << "Sales Amount: " << FIX << SalesAmount << endl

                << endl
                << "Take Home Pay: " << FIX << TakeHomePay << endl
                << "\tGross Earned: \t : " << FIX << GrossEarnedAmount << endl
                << "\tSales Commission : " << FIX << SalesCommission << endl

                << endl;
        }
             
};

int main() {
        
        EmployeesPay Employee;
        string employeeID, employeeName, salesCode, exitChoice;
        double salesAmount;

    while(1) {

        system("clear");

        cout << "Enter Employee ID: ";
        getline(cin, employeeID);
        Employee.setEmployeeID(employeeID);

        cout << "Enter Employee Name: ";
        getline(cin, employeeName);
        Employee.setEmployeeName(employeeName);

        cout << "Enter Sales Code [A/B]: ";
        getline(cin, salesCode);
        
        while ((salesCode != "A") && (salesCode != "a") && (salesCode != "B") && (salesCode != "b")) {

            cout << "Invalid Input! [A/B]: ";
            cin >> salesCode;

            if ((salesCode == "A") && (salesCode == "a") && (salesCode == "B") && (salesCode == "b"))
                break;
                
        }

        Employee.setSalesCode(salesCode);

        cout << "Enter Sales Amount: ";
        cin >> salesAmount;
        cin.get();
        Employee.setSalesAmount(salesAmount);

        // EmployeesPay employee(employeeID, employeeName, salesCode, salesAmount);

        Employee.PrintDetails();
        

        cout << endl << endl << "Do you want to Try Again? [Y/N] ";
        cin >> exitChoice;
        cin.get();

        if ((exitChoice == "Y") || (exitChoice == "y")) 
            continue;

        else {
            
            cout << "\n\nThank you for using this program!";
            break;

        }   
    }
}