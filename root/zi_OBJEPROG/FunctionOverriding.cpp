#include <iostream>

using namespace std;

class Animal
{
public:
    void eat()
    {
        cout << "Eating..." << endl;
    }
};

class Dog : public Animal
{
public:
    void eat()
    { //same method name and parameters from its parent class
        cout << "Eating bread...";
    }
};

class Cat : public Animal
{
public:
    void eat()
    { //same method name and parameters from its parent class
        cout << "Eating fish...";
    }
};

class Owl : public Animal
{
    //will not perform override
};

int main()
{
    /* Reference of base class pointing to
* the object of child class.
*/
    //animal is in the form of dog
    Animal animal = Dog(); //object polymorphism widening
    animal.eat();

    //animal changed as it is now a cat
    animal = Cat();
    animal.eat();

    //morph into Owl
    animal = Owl();
    animal.eat();

    return 0;
}