#include<iostream>
#include<cstring>
using namespace std;

class ParentOne {

    public:

    ParentOne() {

        cout << "ParentOne Constructor";

    }
    ParentOne(string message) {

        cout << "ParentOne Parameterized";

    }

};

class ParentTwo {

    public:

    ParentTwo() {

        cout << "ParentTwo Constructor";

    }
    ParentTwo(string message) {

        cout << "ParentTwo Parameterized";

    }

};

class ParentThree {

    public:

    ParentThree() {

        cout << "ParentThree Constructor";

    }
    ParentThree(string message) {

        cout << "ParentThree Parameterized";

    }

};

class InbredChild : public ParentOne, ParentTwo, ParentThree {

    public:
        InbredChild() {

            cout << "Child Constructor";

        }
        InbredChild (string message) {

            cout << message << endl;

        }

};

int main() {

    InbredChild ChildOne;

    cout << endl << endl;

    InbredChild ChildTwo("Child Parameterized Constructor");

    return 0;

}