


/*

    Core Tenets in OOP
        Polymorphism - function overloading; function overriding
        Inheritance - parent-child or "is a" relationship
        Encapsulation
            Access Specifier
            Access Modifier
        Abstraction

*/