#include <iostream>

using namespace std;

// raymond.zalmeda@iacademy.edu.ph

// UDT -> user-defined type
// pre-defined type made by user


// classes
class Grade {

	public:
	
		// number instance variables
		// input values
		string id;
		string name;
		double midterm;
		double prefinal;

		//computed values
		double FinalGrade;
		string Remarks;

		Grade () {

			// local variables are inside Grade

			id = "000000";
			name = "null";

			midterm = 0;
			prefinal = 0;
			FinalGrade = 0;

			Remarks = "null";

		}

		~Grade() {


		}

		// member functions
		void computeAverage() {

			FinalGrade = midterm * 0.5 + prefinal * 0.5;

		}

		void determineNewRemarks() {

			if(FinalGrade >= 70) 
				Remarks = "Passed";

			else
				Remarks = "Failed";

		}


		void printData() {

			cout << "ID: " << id
				 << "\nName: " << name
				 << "\nMidterm Grade: " << midterm
				 << "\nPrefinal Grade: " << prefinal
				 << "\n\nFinal Grade: " << FinalGrade
				 << "\n\nRemarks: " << Remarks;

		}

}

// Grade id;

int main() {

	Grade aleph;

	cout << "Enter your student ID: ";
	cin >> aleph.id;

	cout << "Enter Name: ";
	getstring(cin, aleph.name);




}

/*

	stack memory -> for primitive types
	heap memory -> objects
	
*/