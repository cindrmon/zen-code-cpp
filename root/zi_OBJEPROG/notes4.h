/*


 Types of Polymorphism
 Function Overloading
 No overriding; no inheritance
 Function Overriding
 Object Polymorphism
 Narrowing
 Widening


    3 types of variable scopes
        local variable -> owned by Function
        instance variable -> owned by object
        static variable -> owned by class

    the class is just a template, the object is an instance of a class

    class SampleClass {

        string aString;
        int anInteger;

    };

    int main() {

        SampleClass anObject;
        SampleClass anObject2;

    }

    : -> inheritance
    :: -> scope resolution/ownership
    . -> access

*/

#include<iostream>
#include<cstring>

class Chocolate{

 private:
 double sugar;
 double spice;
 char everythingNice;

 public:

 Chocolate() {

 std::cout << "this is construction!";

 }
 Chocolate(double SugarContent){

 this->sugar = SugarContent;

 }
 ~Chocolate() {

 std::cout << "This is destruction!";

 }

};