#include<iostream>

using namespace std;

class Calc {

    public:

        // function overloading through different types of arguments with the same method name

        int Addition(int operAnd_1, int operAnd_2) {
            return operAnd_1 + operAnd_2;
        }

        int Addition(int operAnd_1, int operAnd_2, int operAnd_3) { 
            return operAnd_1 + operAnd_2 + operAnd_3;
        }

        double Addition(double operAnd_1, double operAnd_2, double operAnd_3) {
            return operAnd_1 + operAnd_2 + operAnd_3;
        }

        int Addition(int aList[], int ListSize) {

            int Sum=0;

            for(int IDX=0; IDX<ListSize; ++IDX) 
                Sum+=aList[IDX];

            return Sum;

        }

        double Addition(double aList[], double ListSize)
        {

            double Sum=0;

            for (int IDX = 0; IDX < ListSize; ++IDX)
                Sum += aList[IDX];
            
            return Sum;
        }
};

int main() {

    Calc toCalc;
    int aList[6] = {10, 20, 30, 40, 50, 60};
    double C=293.1329;
    float X = static_cast<float>(C);

    cout << "The summation value of 10 and 20 is: "
         << toCalc.Addition(10, 20);

    cout << "The summation values of 10, 20, and 30 are: "
         << toCalc.Addition(10,20,30);

    cout << "The summation values of an array (10, 20, 30, 40, 50, 60) are: "
         << toCalc.Addition(aList, 6);

    cout << endl << X;


    cout << endl << sizeof(double) << endl << sizeof(long);

    

}