#include <iostream>
#include <string>
#include <cstring>
#include <iomanip>

//fixes values to 2 decimal places
#define FIX fixed << setprecision(2)

using namespace std;

class EmployeesPay
{

private:
    // user input
    string EmployeeID;
    string EmployeeName;
    string SalesCode;
    double SalesAmount;

    // additionals
    const double AdditionalSalesCommission = 0.05;

    // output
    double TakeHomePay;
    double GrossEarnedAmount;
    double SalesCommission;

public:
    // init
    EmployeesPay(string EmpID, string EmpName, string SalesCd, double SalesAmt)
    {

        EmployeeID = EmpID;
        EmployeeName = EmpName;
        SalesCode = SalesCd;
        SalesAmount = SalesAmt;

        TakeHomePay = 0;
        GrossEarnedAmount = 0;
        SalesCommission = 0;
    }

    // destructor
    ~EmployeesPay()
    {
    }

    // sales codes computation
    void
    SalesCodeAlpha()
    {

        GrossEarnedAmount = 175 + (0.5 * SalesAmount);
    }

    void SalesCodeBeta()
    {

        GrossEarnedAmount = 100 + (0.2 * SalesAmount);
    }

    // additional sales commission
    void SalesAdditionalCommission()
    {

        if (SalesAmount >= 2500)
            SalesCommission = 0.05 * SalesAmount;
    }

    // compute for take home pay
    void TakeHomePayFinal()
    {

        SalesAdditionalCommission();

        if (SalesCode == "A" || SalesCode == "a")
        {

            SalesCodeAlpha();
            TakeHomePay = GrossEarnedAmount + SalesCommission;
        }
        else if (SalesCode == "B" || SalesCode == "b")
        {

            SalesCodeBeta();
            TakeHomePay = GrossEarnedAmount + SalesCommission;
        }
        else
        {

            cout << "Error!"; // just in case?
        }
    }

    void PrintDetails()
    {

        TakeHomePayFinal();

        system("clear");

        cout
            << "Employee ID: " + EmployeeID << endl
            << "Employee Name: " + EmployeeName << endl
            << "Sales Code: " + SalesCode << endl
            << "Sales Amount: " << FIX << SalesAmount << endl

            << endl
            << "Take Home Pay: " << FIX << TakeHomePay << endl
            << "\tGross Earned: " << FIX << GrossEarnedAmount << endl
            << "\tSales Commission: " << FIX << SalesCommission << endl

            << endl;
    }
};

int main()
{

    string employeeID, employeeName, salesCode, exitChoice;
    double salesAmount;

    while (1)
    {

        system("clear");

        cout << "Enter Employee ID: ";
        getline(cin, employeeID);

        cout << "Enter Employee Name: ";
        getline(cin, employeeName);

        cout << "Enter Sales Code [A/B]: ";
        getline(cin, salesCode);

        while ((salesCode != "A") && (salesCode != "a") && (salesCode != "B") && (salesCode != "b"))
        {

            cout << "Invalid Input! [A/B]: ";
            cin >> salesCode;

            if ((salesCode == "A") && (salesCode == "a") && (salesCode == "B") && (salesCode == "b"))
                break;
        }

        cout << "Enter Sales Amount: ";
        cin >> salesAmount;
        cin.get();

        EmployeesPay employee(employeeID, employeeName, salesCode, salesAmount);

        employee.PrintDetails();

        cout << endl
             << endl
             << "Do you want to Try Again? [Y/N] ";
        cin >> exitChoice;
        cin.get();

        if ((exitChoice == "Y") || (exitChoice == "y"))
            continue;

        else
        {

            cout << "\n\nThank you for using this program!";
            break;
        }
    }
}