#include<iostream>

using namespace std;


class Parent {

    public:

        void Print() {

            cout << "Parent Class" << endl;

        }
};

class Child : public Parent {


    public:

        void Print() {

            cout << "This is now in control of the child class" << endl;

        }

};


class Parent_Overriding{

    public:
        void getData(int Integer){
            cout << "Integer Value Entered: " << Integer << endl;

        }
        void getData(double Decimal){
            cout << "Double Value Entered: " << Decimal << endl;

        }
};




int main() {

    
   Child SampleObject;
   Parent_Overriding AnotherObject;
   int anInteger;
   double aDouble;

    SampleObject.Print();

    cout << endl << endl;

    SampleObject::Parent.print();
    

    cout << "Enter an Integer: "; cin >> anInteger;
    cout << "Enter a Double value: "; cin >> aDouble;

   AnotherObject.getData(anInteger);
   AnotherObject.getData(aDouble);
   
   

    return 0;
}
