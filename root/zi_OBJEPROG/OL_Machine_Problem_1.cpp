#include<iostream>
#include<string>

// constant values
#define USD 42.00
#define JPY 0.65
#define EUR 60.00

using namespace std;

class Convert {

	public:

		//User Input
		double PHPAmt;
		string Currency;

		// output
		double ConvertedValue;

		Convert() {

			PHPAmt = 0;
			Currency = "nul";

		}

		~Convert() {

			// cout << "\n\nProgram Ended";

		}

		void Chooser() {


			// selects if user chooses USD, JPY, or EUR
			if(Currency=="USD") {

				ConvertedValue = PHPAmt/USD;

			}
			else if(Currency=="JPY") {

				ConvertedValue = PHPAmt/JPY;

			}
			else if(Currency=="EUR") {

				ConvertedValue = PHPAmt/EUR;

			}

			// redundancy
			else
				cout << "Invalid Input!";

			// final output
			cout << "The amount converted from PHP " << PHPAmt << " to " << Currency
				 << " is " << ConvertedValue << ".";

		}

};


int main() {

	Convert userChoice;
	string choice;

	while(1) {

		// user input
		cout << "Enter Peso Amount: "; 
		cin >> userChoice.PHPAmt;
		cin.get();

		cout << "Enter Foreign Currency (USD,JPY,EUR)"
			 << "\nNOTE: USD is US Dollar, JPY is Japanese Yen, and EUR is Euro."
			 << "\n>> ";
		cin >> userChoice.Currency;

		// error checking if input for "Foreign Currency" is wrong
		while ( (userChoice.Currency!="USD") && (userChoice.Currency!="JPY") && (userChoice.Currency!="EUR") ) {

			cout << "\n\nInvalid input! Try Again!";

			cout << "\n\nEnter Foreign Currency (USD,JPY,EUR): ";
			cin >> userChoice.Currency;

		}

		userChoice.Chooser();

		// repeat selector thingy
		cout << endl << endl << "Do you wanna try again? (Y/N): ";
		cin >> choice;
		
		if(choice == "Y" || choice == "y")
			continue;

		else {

			cout << endl << endl << "Thank you for using this program!";
			break;

		}
	}
}