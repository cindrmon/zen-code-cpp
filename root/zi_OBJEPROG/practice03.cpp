#include <iostream>

using namespace std;

struct Data {

    int x;
    int y;

    Data operator+ (const Data& e) {

        Data temp;

        temp.x = x + e.x;
        temp.y = y + e.y;

        return(temp);

    }

}d1, d2, d3;

int main() {

    cout << "Enter x and y of data 1: ";
    cin >> d1.x >> d1.y;

    cout << "Enter x and y of data 2: ";
    cin >> d2.x >> d2.y;
 
    d3 = d1+d2;

    cout << "The data of d3 is: " << endl;
    cout << "D3.x = " << d3.x << " and D3.y = " << d3.y << ".";

}

