#include<iostream>
#include<string>

using namespace std;

class Parent {

  public:
    string ParentClassStringMessage;

    Parent() { // regular constructor
        cout << "This is the default Parent Constructor Class." << endl;
    }

    Parent(string Message) { // Parameterized Constructor
        cout << Message << " -> Inside the Parent Class" << endl;
        ParentClassStringMessage = Message;
    }

};

class Child: public Parent {

  public:
    string ChildClassStringMessage;

    Child() {
        cout << "This is the Default Child Constructor Class" << endl;
    }

    Child(string Message):Parent(Message) {
        cout << Message << endl;
    }

};

int main() {

  Child child3();

  return 0;

}