#include <iostream>

using namespace std;

void weewoo(int n) {
	
	if(n<2)
		cout << "woo";
	else {
		cout << "wee ";
		weewoo(n-1);
	}
}

int main() {
	
	int n;
	
	cout << "Enter a number: ";
	cin >> n;
	
	weewoo(n);

	system("pause");
	
}
