#include <iostream>
#include <cstring>

#define MAX_CHAR 100

using namespace std;


int main() {
	
	char string[MAX_CHAR];
	
	cout << "Enter a string: ";
	cin.get(string, MAX_CHAR);
	cin.get();
	
	cout << endl << endl;
	
	for(int i=0; i<strlen(string); ++i)
		cout << string[i];
	
	
	cout << "Autocaps made it like this now: ";
	autoCaps(string);
	
}
