#include <iostream>

using namespace std;

int main() {
	
	int p;
	int *y;
	
	p=0; cout << "p=0" << endl << endl;
	
	cout << "p = " << p << endl;
	cout << "y = " << y << endl;
	cout << "&p = " << &p << endl;
	cout << "&y = " << &y << endl;
	cout << endl << endl << endl << endl;
	
	cout << "p=0" << endl;
	y = &p; cout << "y=&p" << endl << endl;
	
	cout << "p = " << p << endl;
	cout << "y = " << y << endl;
	cout << "&p = " << &p << endl;
	cout << "&y = " << &y << endl;
	cout << endl << endl << endl << endl;
	
	int x;
	
	y = &x; cout << "y=&x" << endl << endl;
	
	cout << "p = " << p << endl;
	cout << "y = " << y << endl;
	cout << "&p = " << &p << endl;
	cout << "&y = " << &y << endl;
	cout << "x = " << x << endl;
	cout << "&x = " << &x << endl;
	cout << endl << endl << endl << endl;
		
	
}

//therefore, pointers only store hexadecimal values (memory addresses)

//storing hexadecimal values: pointerVariable = &variable

//printing a pointer variable will print the hexadecimal value that the pointer is
//pointing to, which is the variable

//y = &p

// VARIABLE		MEMORY ADDRESS		VALUE
// p			0x6ffe0c			0
// *y			0x6ffe00			0x6ffe0c --> mem address of var p
