#include <iostream>

using namespace std;

int main() {

    int *ptr, var, **pitrpatr;

    var = 10; // level 0

    ptr = &var; // level -1

    pitrpatr = &ptr; // level -2

    cout << endl
         << var << endl // 10
         << ptr << endl // address of var
         << pitrpatr; // address of ptr

    cout << endl << endl
         <<&var << endl // revealing the address of var
         <<&ptr << endl // revealing the address of ptr
         <<pitrpatr; // address of ptr

    cout << endl << endl
         <<*pitrpatr << endl //address of pointer pointing to
         <<**pitrpatr << endl; // value of pointer pointing to

}