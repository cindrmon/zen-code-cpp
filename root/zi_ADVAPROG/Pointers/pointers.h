/*

    int var; 0x1001
    int *p; 0x5001
                        p=1001
                        &p=5001

    such that

    1000    
    1001    var
    1002    var
    1003    var
    1004    var
    1005
    ...
    5000
    5001    *P
    5002    *p
    5003    *p
    5004    *p
    5005    *p
    5006    *p
    5007    *p
    5008    *p
    5009
    5010
    ...


    * - "The contents of..."; 
    & - "The address of.."

*/
