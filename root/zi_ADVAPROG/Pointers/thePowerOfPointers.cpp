#include <iostream>
#define ARRAY_SIZE 10

using namespace std;

void iWillCountTheNumberOfOddInYourArray(int *array, int *oddSum) {

    *oddSum = 0;

    for(int i=0; i < ARRAY_SIZE; ++i) {

        if ((*(array + i) % 2) != 0)
            ++*oddSum;
    }

}

int main() {

    int array[ARRAY_SIZE] = {0}, *sum;
    char exit;

    for (int i = 0; i < ARRAY_SIZE; ++i) {

        cout << "Enter integer " << i+1 << ": ";
        cin >> array[i];
    }

    iWillCountTheNumberOfOddInYourArray(array, sum);

    cout << endl << endl
         << "The total numnber of odd numbers are " << *sum << ".";

    cout << endl << endl
         << "Do you wanna try again? ";
    cin >> exit;

    if (isupper(exit))
        exit = tolower(exit);

    while(exit=='y') {

        for (int i=0; i < ARRAY_SIZE; ++i) {

            cout << "Enter integer "  <<i << ": ";
            cin >> array[i];

        }

        iWillCountTheNumberOfOddInYourArray(array,sum);

        cout << endl << endl << "The total numnber of odd numbers are " << *sum << ".";

        cout << endl << endl << "Do you wanna try again? ";
        cin >> exit;

        if(isupper(exit))
            exit = tolower(exit);

    }

    cout << endl
         << "Thanks for using program.";


}