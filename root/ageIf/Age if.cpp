#include <iostream>
#include <string.h>

using namespace std;

int main()
{
    string name;
    int age, year; /* Variables */

    cout << "Enter name: ";
    getline(cin,name);

    cout << "Enter the year of your birth: ";
    cin >> year;

    age = 2020 - year;

    cout << name << " your age is " << age << ".\n";

    if(age >= 40)
        cout << "Ang tanda mo na, " << name << ".\n";
    else
        cout << "Bata ka pa " << name << ".\n";


    return 0;
}
