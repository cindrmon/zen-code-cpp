/*

This is legitimately my first time using object-oriented concepts or whatever
This is also an attempt to recreate my old C program of a voting system that 
I did during Grade 10, and its been long since I haven't touched it. So far, 
the stuff I was only able to recreate is the algorithm itself, without the
character input for entering the response to exiting the program proper, and
the character input to choose which party/candidate they want to vote. I know
I'm not allowed to write an entire backstory in a program like this, but this
is just a heads-up to my future self ig, or anyone who would want a context on
this crappy program, and I highly doubt other programmers or people would read
this or dabble in this program or whatever, cuz no one really cares about a 
simple voting program that was made 3 years ago, and I know all my other class
mates have done way more advanced stuff than what we did during our Computer
class, and idk anymore.

*/

#include <iostream>

//these are arbitrary, cuz this will be used in the future when i'll try recreating this program
#include <string>
#include <cstring>

using namespace std;

/*
candidate tally object thingy whatever 
idrk what this is, but it works an its rly awesome to use
*/
class candidateTally {
	
	//no touchie the sensitive data... pls no 
	private:
		int candN;
		
	/*
	will be distributed or used by the int main() function to calculate each
	candidates' votes
	*/
	public:
		
		//adds one vote per tally
		int tallyAdder() {
			return ++candN;
		}
		
		//shows the results in value form or smth
		void candTotalShout() {	
			cout << candN;	
		}
	
}cand1, cand2, cand3;
/*
These three are the objects under the class candidateTally, which gives different
values for each object or whatever
*/

int main() {
	
	int choice;
	int total = 0;
	int exit;
	
	//sentinel-controlled loop for checking prompt if another user would vote again
	while(strcpy(choice, exit) != 0){
		
		//MAIN INPUT
		system("cls");
	
		cout << "Type 1 for candidate 1; 2 for candidate 2, and 3 for candidate 3: " << endl;
		cin >> choice;
		
		//checks if the candidate value is beyond the range given
		while( (choice >= 4) || (choice <= 0) ){
			
			system("cls");
			
			cout << "Invalid input! Please try again." << endl;
			cout << "Type 1 for candidate 1; 2 for candidate 2, and 3 for candidate 3: " << endl;
			cin >> choice;
		} 
		
		//switch to choose which object to add
		switch(choice) {
			
			case 1:
				cand1.tallyAdder(); break;
				
			case 2:
				cand2.tallyAdder(); break;
				
			case 3:
				cand3.tallyAdder(); break;
				
			default:
				cout << "Invalid input! Please try again." << endl;
			
		}
		
		//increments the number of voters per loop
		++total;
		
		//shows the current number of votes and voters per loop
		system("cls");
		
		cout << endl << "Total number of voters: " << total;
		cout << endl << "Total number of votes for candidate 1: "; cand1.candTotalShout();
		cout << endl << "Total number of votes for candidate 2: "; cand2.candTotalShout();
		cout << endl << "Total number of votes for candidate 3: "; cand3.candTotalShout();
		
		cout << endl << "Thanks for voting! Next please.";
		cout << endl;
		
		//prompts the user to exit the program and show the final results
		cout << endl << "Type 0 to exit the program and to give the final results, otherwise, type 1: ";
		getline(cin,,choice);
	
	}
	
	//shows the final results and exits the program
	system("cls");
	
	cout << endl << "Final tally of voters: " << total;
	cout << endl << "Final tally of candidate 1: "; cand1.candTotalShout();
	cout << endl << "Final tally of candidate 2: "; cand2.candTotalShout();
	cout << endl << "Final tally of candidate 3: "; cand3.candTotalShout();
	cout << endl << endl;
	cout << "Thanks for using this program!!!";
	
}