#include <iostream>
#include <cmath>

using namespace std;

void determineArmstrong(int num, int ctr)
{
    int r, temp, sum{};

    temp = num;
    while (num > 0)
    {
        r = num % 10;
        sum = r + pow(r, ctr);
        num = num / 10;
        ++ctr;
    }

    if (temp == sum)
    {
        cout << "\n\nNumber is an Armstrong number.\n\n";
    }
    else 
    {
        cout << "\n\nNumber is not an Armstrong number.\n\n";
    }
}

int main()
{
     int num, ctr, r;

    cout << "Enter a number: ";
    cin >> num;

    ctr = 0;
    while (num > 0)
    {
        r = num % 10;
        num = num / 10;
        ++ctr;
    }

    determineArmstrong(num, ctr);

    return 0;
}