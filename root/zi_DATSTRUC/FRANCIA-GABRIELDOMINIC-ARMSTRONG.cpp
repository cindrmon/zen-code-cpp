#include <iostream>
#include <cmath>
#define MAX_VALUE 100

/*

    To get the last digit, x%10
    to get the first digit, x/100
    to get the middle digit/s, x/100%10; x/10%10

*/

using namespace std;

void armstrong(int num) {

    int value=0, tempNum, actualNum;
    tempNum = num;
    actualNum = num;


    //counts the numbers in the value of num
    while(tempNum!=0){

            tempNum/=10;
            ++value;

    }

    int VALUE = value;
    int digit[VALUE]={0};
    int sum=0;

    //searches for each value in an array and
    //raises it to value and adds the array
    for(int i=0; i<VALUE; ++i){

        digit[i] = num%10;
        num /= 10;
        
        digit[i] = pow(digit[i],VALUE);

        sum += digit[i];

    }


    //tests if number is an armstrong number or not
    if(sum==actualNum)
        cout << endl
             << endl
             << "This is an armstrong number.";
    else
        cout << endl
             << endl
             << "This is not an armstrong number.";
}

int main() {

    int number;

    cout << "ENTER A VALUE: ";
    cin >> number;

    armstrong(number);


}

