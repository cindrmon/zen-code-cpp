#include<iostream>

#define MAX_SIZE 100

using namespace std;

// QUEUES_Operations

    bool QueueListFull(int Alpha, int Omega) {

        if(Omega == MAX_SIZE-1)
            Omega = 0; // makes circular motion by assigning Omega to the start of the list
        else 
            ++Omega;

        if(Omega == Alpha)
            return 1;
        else
            return 0;
    }

    bool QueueListEmpty(int Alpha, int Omega) {

        if(Alpha == Omega)
            return 1;
        else 
            return 0;

    }

    void QueueInsertItem(int QueueList[], int Alpha, int *AddrOmega, int ItemAdd) {

        if(QueueListFull(Alpha, *AddrOmega))
            cout << "The List is full!";

        else{

            if(*AddrOmega == MAX_SIZE-1)
                *AddrOmega = 0;

            else   
                ++(*AddrOmega);

            QueueList[*AddrOmega] = ItemAdd;
            cout << "\nItem inserted in queue.";

        }

    }

    int QueueRemoveItem(int QueueList[], int *AddrAlpha, int Omega){

        int Item;

        if(QueueListEmpty(*AddrAlpha,Omega))
            cout << "The list is empty!";

        else{

            if(*AddrAlpha == MAX_SIZE-1)
                *AddrAlpha = 0;

            else 
                ++(*AddrAlpha);

            Item = QueueList[*AddrAlpha];
            cout << "\nItem removed from the queue.";

            return Item;

        }

    }

    int QueueListAlphaItem(int QueueList[], int Alpha, int Omega) {

        if(QueueListEmpty(Alpha,Omega))
            cout << "\nThe list is empty!";

        else {
            if(Alpha == MAX_SIZE-1)
                return (QueueList[0]);

            else
                return (QueueList[Alpha+1]);

        }

    }

// Stacks Operations

    bool StackListEmpty(int TopTier){

        if(TopTier < 0)
            return 1;

        else 
            return 0;

    }

    bool StackListFull(int TopTier){

        if(TopTier == (MAX_SIZE-1))
            return 1;
        
        else 
            return 0;

    }  

    void StackListPushRequest(int StackList[], int *AddrTopTier, int ItemAdd) {

        if(StackListFull(*AddrTopTier))
            cout << "\nThe Stack List is full!";

        else {

            ++(*AddrTopTier);
            StackList[*AddrTopTier] = ItemAdd;

            cout << "\nItem inserted in the stack.";

        }

    } 

    int StackListPullRequest(int StackList[], int *AddTopTier) {

        int Item;

        if(StackListEmpty(*AddrTopTier))
            cout << "\nThe Stack List is empty!";

        else {

            Item = StackList[*AddrTopTier];
            --(*AddrTopTier);
            cout << "\nItem removed from the Stack.";

            return Item;

        }

    }

    int StackListTopTier(int StackList[], int TopTier) {

        if(StackListEmpty(TopTier))
            cout << "\nThe Stack List is empty!";

        else 
            return StackList[TopTier];

    }

int main() {

    int Queue[MAX_SIZE];
    int Front,Back;

}

