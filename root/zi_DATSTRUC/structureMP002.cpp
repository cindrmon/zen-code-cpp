#include <iostream>

using namespace std;

struct studentData {

    char name[51];
    int score;
    float grade;

} students[5];

void inputData() {

    studentData *InputDataPointer;


    for(InputDataPointer = students; InputDataPointer < &students[5]; ++InputDataPointer ) {

        int i=1;

        cout << "Enter name of student " << i << ": ";
        cin.get(InputDataPointer -> name, 51);
        cin.get();

        cout << "Enter Score of student " << i << ": ";
        cin >> InputDataPointer -> score;

        cout << "Enter grade of student " << i << ": ";
        cin >> InputDataPointer -> grade;

        cin.get();

        ++i;  

    }

}

void outputData() {

    studentData *OutputDataPointer;

    
    for (OutputDataPointer = students; OutputDataPointer < &students[5]; ++OutputDataPointer){

        int i = 1;

        cout << endl << endl;

        cout << "===========================================================================" << endl;
        cout << "Data for Student " << i << ":" << endl;
        //cout << "Name: " << OutputDataPointer -> name << endl;
        cout << "Name: " << students[i].name << endl;
        //cout << "Score: " << OutputDataPointer -> score << endl;
        cout << "Score: " << students[i].score << endl;
        //cout << "Grade: " << OutputDataPointer -> grade << endl;
        cout << "Grade: " << students[i].grade << endl;
        cout << "===========================================================================" << endl;

        //++i;
    }
}

void highestTestScore(){

    //students[i].score

    int highScore = 0;

    for (int i = 0; i < 5; ++i)
    {

        if (students[i].score > highScore)
            highScore = students[i].score;
    }

    for (int i = 0; i < 5; ++i)
    {

        if (highScore == students[i].score)
            cout << students[i].name;
        break;
    }

    cout << "===========================================================================" << endl;
    cout << "The highest score is " << highScore << ", and their name is "; 
        for (int i = 0; i < 5; ++i){

            if (highScore == students[i].score)
                cout << students[i].name;
        }
}



int main () {

    inputData();

    cout << endl;

    outputData();

    cout << endl;

    highestTestScore();

}
