#include<iostream>

#define MAX_SIZE 1000
#define CLEAR "cls"
// #define CLEAR "clear"    // if the clear command doesn't work, change to cls for windows

using namespace std;

typedef int LIST_TYPE; // what the data of the list should contain; data type editable
typedef int LIST_SIZE; // what is the current size of the current list; DO NOT EDIT DATATYPE
typedef int ETC;       // other purposes, like position; DO NOT EDIT DATATYPE
typedef int CHOICE;    // for user-inputted choice or whatever; data type editable

// tester functions:
    bool ListIsFull(LIST_SIZE ListSize){

        if(ListSize==MAX_SIZE-1)
            return true;
        else
            return false;

    }

    bool ListIsEmpty(LIST_SIZE ListSize){

        if(ListSize<0)
            return true;
        else
            return false;

    }

// Operator Functions

    bool IsPrime(LIST_TYPE aNumber){

        int Primer = 0;

        if (aNumber == 1)
            return 0;

        for (ETC I = 1; I <= aNumber; ++I){

            if (((aNumber % I) == 0))
                ++Primer;
        }

        if (Primer == 2)
            return 1;
        else
            return 0;
    }

// item manipulation:

    void ListAddItems(LIST_TYPE aList[], LIST_TYPE new_data, ETC ListItem, LIST_SIZE *addrListSize)
    {
        ETC I;

        if (ListIsFull(*addrListSize))
            cout << "\nThe List is Full!";

        else {

            // moves data to clear out space for new data to enter
            for (I = *addrListSize; I >= ListItem; --I)
                aList[I + 1] = aList[I];

            // inputs new data
            aList[ListItem] = new_data;

            // adds the counter to increase list size
            ++(*addrListSize);

            // output text to notify that the item is added
            cout << "\nItem Successfully Added!";
        }
    }

    void ListDeleteItems(LIST_TYPE aList[], ETC ListItem, LIST_SIZE *addrListSize) {

        ETC I;

        if (ListIsEmpty(*addrListSize))
            cout << "\nThe List is Empty!";

        else {

            // moves data to fill in the space that the deleted data left
            for (I = ListItem + 1; I <= *addrListSize; ++I)
                aList[I - 1] = aList[I];

            // subtracts the counter to decrease list size
            --(*addrListSize);

            // output text to notify that the item is deleted
            cout << "\nItem Successfully Deleted!";

        }

    }

// List Outputs:

    // Output Manipulation:

    void ListLocateItems(LIST_TYPE aList[], LIST_TYPE SearchData, LIST_SIZE ListSize){

        ETC I=0;

        if(ListIsEmpty(ListSize))
            cout << "The list is empty!";

        else {

            while ((I != ListSize + 1) && (aList[I] != SearchData)) {
                
                ++I;

                if (I != ListSize + 1)
                    cout << "\nItem Requested is Item " << I + 1 << ".";
                else
                    cout << "\nItem Does Not Exist.";

            }
        }
    }

    // Additional Functions

    void ListEvenNumbers(LIST_TYPE aList[], LIST_SIZE ListSize) {

        ETC I;
        ETC SumEven=0;

        if(ListIsEmpty(ListSize))
            cout << "The List is Empty!" << endl;

        else{

            for(I=0; I<=ListSize; ++I)
                if((aList[I]%2)==0)
                    ++SumEven;

            cout << "There are " << SumEven << " Even Numbers.";

            if(SumEven>0) {
                cout << endl << "The numbers are:" << endl;

                for(I=0; I<=ListSize; ++I)
                    if((aList[I]%2)==0)
                        cout << aList[I] << " ";
                
            }

            cout << endl << endl;
        }

    }

    void ListPrimeNumbers(LIST_TYPE aList[], LIST_SIZE ListSize) {

        ETC I, SumPrime=0;

        if(ListIsEmpty(ListSize))
            cout << "The List is Empty!" << endl;

        else{

            for(I=0; I<=ListSize; ++I)
                if(IsPrime(aList[I]))
                    ++SumPrime;

            cout << "There are " << SumPrime << " Prime Numbers.";

            if (SumPrime > 0){
                cout << endl
                     << "The numbers are:" << endl;

                for(I=0; I<=ListSize; ++I)
                    if(IsPrime(aList[I]))
                        cout << aList[I] << " ";

            }

            cout << endl << endl;
        }

    }

    // Prints Items
    void ListPrintItems(LIST_TYPE aList[], LIST_SIZE ListSize){
        ETC I;

        if (ListIsEmpty(ListSize))
            cout << "\nThe List is Empty!";

        else
            for (I = 0; I <= ListSize; ++I)
                cout << "\nThe Value of Item " << I + 1
                     << " = " << aList[I] << ".";

    }

    // Count List:
    LIST_SIZE ListCount(LIST_SIZE ListSize){

        return (ListSize+1);

    }

// Main Output:
    void MainMenu(LIST_SIZE ListSize) {

        cout << endl
            << "LIST (ARRAY) PROGRAM";
        cout << endl
            << "---------------------";
        cout << endl
            << "There are currently " << ListCount(ListSize) << " items in the list.";
        cout << endl
            << "Options: ";
        cout << endl
            << "<1> ADD ITEM";
        cout << endl
            << "<2> DEL ITEM";
        cout << endl
            << "<3> LOCATE ITEM";
        cout << endl
            << "<4> PRINT ITEM";
        cout << endl
            << "<5> COUNT AND PRINT EVEN NUMBERS";
        cout << endl
            << "<6> COUNT AND PRINT PRIME NUBERS";
        cout << endl
            << "<7> EXIT PROGRAM";
        cout << endl << endl;
    }

    CHOICE ChoicePicker() {

        CHOICE Choice;

        cout << "Enter the number of your choice: ";
        cin >> Choice;

        while((Choice<1)||(Choice>7)) {

            cout << "Invalid Choice! Please try again!\n"
                 << "Enter the number of your choice (1-7): ";
            cin >> Choice;

        }

        return Choice;

    }

int main() {

    LIST_TYPE List[MAX_SIZE] = {0}, NewData, SearchData;
    LIST_SIZE ListSize=-1, Position;
    CHOICE Choice;

    do{

        MainMenu(ListSize);
        Choice = ChoicePicker();

        switch(Choice){

            case 1: // add an item

                system(CLEAR);

                cout << endl
                     << "Enter position: ";
                cin >> Position;

                    while (Position < 1 || Position > ListCount(ListSize) + 1){
                        cout << endl
                            << "POSITION IS INVALID... TRY AGAIN!!!";
                        cout << endl
                            << "Enter position: ";
                        cin >> Position;
                    }

                cout << endl
                     << "Enter new data: ";
                cin >> NewData;

                ListAddItems(List, NewData, Position - 1, &ListSize);

                break;
            
            case 2: // delete an item

                system(CLEAR);

                if(ListIsEmpty(ListSize)) {
                    cout << endl << "The List is Empty!" << endl;
                    break;
                }

                else{

                    cout << endl
                        << "Enter position to delete: ";
                    cin >> Position;
                    while (Position < 1 || Position > ListCount(ListSize)){
                        cout << endl
                            << "POSITION IS INVALID... TRY AGAIN!!!";
                        cout << endl
                            << "Enter position: ";
                        cin >> Position;
                    }

                    ListDeleteItems(List, Position - 1, &ListSize);
                    break;
                }


            case 3: // locate an item

                system(CLEAR);

                if(ListIsEmpty(ListSize)) {
                    cout << endl << "The List is Empty!" << endl;
                    break;
                }


                else {

                    cout << "\nEnter Search Data: ";
                    cin >> SearchData;

                    ListLocateItems(List,SearchData,ListSize);

                    break;

                }

            case 4: // print all items

                system(CLEAR);

                ListPrintItems(List, ListSize);
                break;

            case 5: // count and print all even numbers in data

                system(CLEAR);

                ListEvenNumbers(List, ListSize);
                break;

            case 6: // count and print all prime numbers in data

                system(CLEAR);

                ListPrimeNumbers(List, ListSize);
                break;

            default:

                system(CLEAR);

                cout << "\nTHANK YOU FOR USING THIS PROGRAM";
                cout << "\nGOOD BYE!!!";

        }
    }while(Choice!=7);
    

}