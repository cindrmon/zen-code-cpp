#include <iostream>
using namespace std;
const int MAX_SIZE = 1000;
typedef int ELEMENTTYPE;

int list_full(int last)
{
    if (last == MAX_SIZE - 1)
        return (1);
    else
        return (0);
}

int list_empty(int last)
{
    if (last < 0)
        return (1);
    else
        return (0);
}

void print_items(ELEMENTTYPE list[], int last)
{
    int index;

    if (list_empty(last) == 1)
        cout << "\nThe List is Empty!";
    else
        for (index = 0; index <= last; ++index)
            cout << "\nThe Value of Item " << index + 1
                 << " = " << list[index] << ".";
}

void locate_item(ELEMENTTYPE list[], ELEMENTTYPE search_data, int last)
{
    int index;

    if (list_empty(last) == 1)
        cout << "\nThe List is Empty!";
    else
    {
        index = 0;
        while (index != last + 1 && list[index] != search_data)
            ++index;

        if (index != last + 1)
            cout << "\nItem Requested is Item " << index + 1 << ".";
        else
            cout << "\nItem Does Not Exist.";
    }
}

void insert_item(ELEMENTTYPE list[], ELEMENTTYPE new_data, int pos, int *ptr_last)
{
    int index;

    if (list_full(*ptr_last) == 1)
        cout << "\nThe List is Full!";
    else
    {
        for (index = *ptr_last; index >= pos; --index)
            list[index + 1] = list[index];

        list[pos] = new_data;
        ++(*ptr_last);
        cout << "\nItem Successfully Added!";
    }
}

void delete_item(ELEMENTTYPE list[], int pos, int *ptr_last)
{
    int index;

    if (list_empty(*ptr_last) == 1)
        cout << "\nThe List is Empty!";
    else
    {
        for (index = pos + 1; index <= *ptr_last; ++index)
            list[index - 1] = list[index];

        --(*ptr_last);
        cout << "\nItem Successfully Deleted!";
    }
}
int count_list(int last)
{
    return (last + 1);
}

// additional programs


int doMenu(int last)
{
    int choice;
    cout << endl
         << "LIST (ARRAY) PROGRAM";
    cout << endl
         << "---------------------";
    cout << endl
         << "There are currently " << count_list(last) << " items in the list.";
    cout << endl
         << "Options: ";
    cout << endl
         << "<1> ADD ITEM";
    cout << endl
         << "<2> DEL ITEM";
    cout << endl
         << "<3> LOCATE ITEM";
    cout << endl
         << "<4> PRINT ITEM";
    cout << endl
         << "<5> EXIT PROGRAM";
    cout << endl
         << "Enter the number of your choice: ";
    cin >> choice;

    while (choice < 1 || choice > 5)
    {
        cout << endl
             << "INVALID INPUT... TRY AGAIN!!!";
        cout << endl
             << "Enter the number of your choice: ";
        cin >> choice;
    }

    return choice;
}

main()
{
    ELEMENTTYPE list[MAX_SIZE] = {0};
    int last = -1, choice, new_data, pos, search_data;

    do
    {
        choice = doMenu(last);
        //add,del,locate,print

        if (choice == 1)
        {
            //insert_item (ELEMENTTYPE list[], ELEMENTTYPE new_data, int pos, int *ptr_last)
            cout << endl
                 << "Enter new data: ";
            cin >> new_data;
            cout << endl



                 << "Enter position: ";
            cin >> pos;


            while(pos>count_list(last)+1 || pos<1) {

                cout << "\nInvalid Position! must be less than " << last+1;
                cout << "\nPlease reenter position: ";
                cin >> pos;

            }

            insert_item(list, new_data, pos - 1, &last);
        }
        else if (choice == 2)
        {
            //delete_item (ELEMENTTYPE list [], int pos, int *ptr_last)
            cout << endl
                 << "Enter position to delete: ";
            cin >> pos;

            while (pos >count_list(last)+1 || pos<1){

                cout << "\nInvalid Position! must be less than " << last+1;
                cout << "\nPlease reenter position: ";
                cin >> pos;
            }

            delete_item(list, pos - 1, &last);
        }
        else if (choice == 3)
        {
            //locate_item (ELEMENTTYPE list[],ELEMENTTYPE search_data, int last)
            cout << endl
                 << "Enter search data: ";
            cin >> search_data;
            locate_item(list, search_data, last);
        }
        else if (choice == 4)
        {
            //print_items(ELEMENTTYPE list[], int last)
            print_items(list, last);
        }
        else
        {
            cout << "\nTHANK YOU FOR USING THIS PROGRAM";
            cout << "\nGOOD BYE!!!";
        }

    } while (choice != 5);
}