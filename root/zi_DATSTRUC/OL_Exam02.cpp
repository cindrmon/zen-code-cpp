#include<iostream>

using namespace std;

struct NodeStructure{

    int value;
    NodeStructure *Next;

};

int FindMax(NodeStructure **Start) {

    // a platform pointer traverses the list of pointers through a linked list
    NodeStructure *Platform; 
    int MaxNumber=0;

    // initializes the platform pointer to start at the Start pointer
    Platform = *Start;

    // while loop to traverse the list to look for the largest number in the list
    while(Platform != NULL) {

        if(MaxNumber < Platform->value)
            Platform->value = MaxNumber;

        // traverses the linked list by moving the Platform pointer to the Next node
        Platform = Platform->Next;

    }

    return MaxNumber;

}