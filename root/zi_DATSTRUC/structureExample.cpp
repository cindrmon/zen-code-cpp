#include <iostream>
#define ARRAY_SIZE 5

using namespace std;

/*

    struct <structureName> {

        type memberName1;
        type memberName2;
        type memberName3;
        ...
        type memberNameN;

    };


    int main () {

        structureName variableName1, variableName2;

        variableName1.memberName1. = <value>;
        variableName1.memberName2 = <value>;
        ...

        variableName2.memberName1 = <value>;
        variableName2.memberName2 = <value>;
        ...

    }

*/

struct bookInfo {

    char bookTitle[51];
    char bookAuthor[51];
    int bookID;

};

int main() {

    bookInfo book[ARRAY_SIZE];

    for(int i=0; i<ARRAY_SIZE; ++i) {


        cout << "Enter title of book " << i+1 << ": ";
        cin.get(book[i].bookTitle,50);
        cin.get();

        cout << "Enter author of book " << i+1 << ": ";
        cin.get(book[i].bookAuthor,50);
        cin.get();

        cout << "Enter the book id of book " << i+1 << ": ";
        cin >> book[i].bookID;
        cin.get();

    }

    //output
    system("cls");

    for(int i=0; i<ARRAY_SIZE; ++i) {


        cout << "========================================" << endl;
        cout << "BOOK " << i+1 << ": " << endl;
        cout << book[i].bookTitle << endl;
        cout << "By: " << book[i].bookAuthor << endl;
        cout << "Book ID: " << book[i].bookID << endl;
        cout << "========================================" << endl;

    }

}