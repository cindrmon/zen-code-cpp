#include <iostream>
#include "structures.h"

using namespace std;

void inputData()
{

    //studentData *InputDataPointer;

    for (int i = 0; i < 5; ++i)
    {

        //for(InputDataPointer = students; InputDataPointer < &students[5]; ++InputDataPointer ) {

        //int i=1;

        cout << "Enter name of student " << i << ": ";
        cin.get(students[i].name, 51);
        //cin.get(InputDataPointer -> name, 51);
        cin.get();

        cout << "Enter Score of student " << i << ": ";
        cin >> students[i].score;
        //cin >> InputDataPointer -> score;

        cout << "Enter grade of student " << i << ": ";
        cin >> students[i].grade;
        //cin >> InputDataPointer -> grade;

        cin.get();

        //++i;
    }
}

void outputData()
{

    //studentData *OutputDataPointer;

    for (int i = 0; i < 5; ++i)
    {
        //for (OutputDataPointer = students; OutputDataPointer < &students[5]; ++OutputDataPointer){

        //int i = 1;

        cout << endl
             << endl;

        cout << "===========================================================================" << endl;
        cout << "Data for Student " << i << ":" << endl;
        //cout << "Name: " << OutputDataPointer -> name << endl;
        cout << "Name: " << students[i].name << endl;
        //cout << "Score: " << OutputDataPointer -> score << endl;
        cout << "Score: " << students[i].score << endl;
        //cout << "Grade: " << OutputDataPointer -> grade << endl;
        cout << "Grade: " << students[i].grade << endl;
        cout << "===========================================================================" << endl;

        //++i;
    }
}
