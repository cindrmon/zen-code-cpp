#include <iostream>

using namespace std;

struct studentData{

    char name[51];
    int score;
    double grade;

} students[5];

double scoreConverter(int score) {

    if(score<70)
        return 5.00;

    else if(score<74)
        return 3.00;

    else if(score<78)
        return 2.75;

    else if(score<81)
        return 2.50;

    else if (score<84)
        return 2.25;

    else if(score<89)
        return 2.00;

    else if(score<91)
        return 1.75;

    else if(score<94)
        return 1.50;

    else if(score<98)
        return 1.25;

    else
        return 1.00;
    

}

void inputData() {

    studentData *iDP;
    int i=1;

    for(iDP=students; iDP<&students[5]; ++iDP) {


        cout << "Enter Student Name no. " << i << ": ";
        cin.get(iDP -> name, 51);
        cin.get();

        cout << "Enter Student Grade no. " << i << ": ";
        cin >> iDP -> score;
        cin.get();

        iDP -> grade = scoreConverter(iDP->score);

        ++i;
        


    }

}

void outputData () {

    studentData *oDP;
    int i = 1;

    for(oDP=students; oDP<&students[5]; ++oDP) {


        cout << endl
             << endl;

        cout << "===========================================================================" << endl;
        cout << "Data for Student " << i << ":" << endl;
        cout << "Name: " << oDP -> name << endl;
        cout << "Score: " << oDP -> score << endl;
        cout << "Grade: " << oDP -> grade << endl;
        cout << "===========================================================================" << endl;
        ++i;

    }

}

void highestScores() {

    studentData *hSP;

    cout << "===========================================================================" << endl;

    int highScore = 0;
    
    for(hSP=students; hSP<&students[5]; ++hSP) {


        if(hSP->score > highScore) 
            highScore = hSP->score;

    }

    cout << "These are the students with the highest score of (" << highScore << "): " << endl;

    for(hSP=students; hSP<&students[5]; ++hSP) {

        if(highScore == hSP->score)
            cout << hSP->name << endl;


    }

    cout << "===========================================================================" << endl;

}

int main() {

    system("cls");

    inputData();

    system("cls");

    outputData();
    highestScores();

    system("pause");


}