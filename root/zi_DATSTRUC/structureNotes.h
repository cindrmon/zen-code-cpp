#include <iostream>

using namespace std;

/*

    struct <structureName> {

        type memberName1;
        type memberName2;
        type memberName3;
        ...
        type memberNameN;

    };


    int main () {

        structureName variableName1, variableName2;

        variableName1.memberName1. = <value>;
        variableName1.memberName2 = <value>;
        ...

        variableName2.memberName1 = <value>;
        variableName2.memberName2 = <value>;
        ...

    }

    accessing bookID:

        book1.bookID = 12345;

        with pointers:

        p = &book1;

        (*p).bookID = 12345;
        p -> bookID = 12345;


    structureName process (structureName x) {

        arguments...

        return x;

    }

*/

struct bookInfo {

    char bookTitle[51];
    char bookAuthor[51];
    int bookID;

};

int main() {

    bookInfo book1;


    cout << "Enter the first book name title: ";
    cin.get(book1.bookTitle,50);
    cin.get();

    cout << "Enter the first book name author: ";
    cin.get(book1.bookAuthor,50);
    cin.get();

    cout << "Enter the book id of the first book: ";
    cin >> book1.bookID;
    cin.get();

    cout << "========================================" << endl;
    cout << "BOOK 1: " << endl;
    cout << book1.bookTitle << endl;
    cout << "By: " << book1.bookAuthor << endl;
    cout << "Book ID: " << book1.bookID << endl;
    cout << "========================================" << endl;


}