#include <iostream>
#include <cstring>
#include <fstream>

using namespace std;


struct student // this is a template
{
	int sId;
	string sName;
	string sCourse;
	string sYear;
	student* next;
};

struct data // this is where data comes from
{
	int dId;
	char dName[50];
	char dCourse[50];
	char dYear[50];
}te;


student* front = NULL, * rear = NULL, * tmp;


void search()
{
	system("cls");
	bool search_id = false;
	tmp = front;
	cout << "Enter Student ID: ";
	cin >> te.dId;
	while (tmp != NULL)
	{
		if (tmp->sId == te.dId)
		{
			search_id = true;
			break;
		}
		tmp = tmp->next;
	};

	if (search_id == true)
	{
		cout << endl << "Student ID: " << tmp->sId << endl;
		cout << "Student Name: " << tmp->sName << endl;
		cout << "Student Course: " << tmp->sCourse << endl;
		cout << "Student Year: " << tmp->sYear;
		system("pause>0");
	}
	else
	{
		cout << "Student Not Found";
		system("pause>0");
	}

	search_id = false;
	tmp = front;
}

void insert()
{
	cout << "Student ID: ";
	cin >> te.dId;

	cin.get();

	cout << "Student Name: ";
	cin.get(te.dName, 50);

	cin.get();

	cout << "Student Course: ";
	cin.get(te.dCourse, 50);

	cin.get();

	cout << "Student Year Level: ";
	cin.get(te.dYear, 50);
	cin.get();

	if (rear == NULL)
	{
		rear = new student;
		rear->sId = te.dId;
		rear->sName = te.dName;
		rear->sCourse = te.dCourse;
		rear->sYear = te.dYear;
		rear->next = NULL;
		front = rear;
	}
	else
	{
		tmp = new student;
		rear->next = tmp;
		tmp->sId = te.dId;
		tmp->sName = te.dName;
		tmp->sCourse = te.dCourse;
		tmp->sYear = te.dYear;
		tmp->next = NULL;
		rear = tmp;
	}

	ofstream myfile;
	myfile.open("Student Data.txt", ios::out | ios::app);
	myfile << "\tStudent ID: " << te.dId << endl;
	myfile << "\tStudent Name: " << te.dName << endl;
	myfile << "\tStudent Course: " << te.dCourse << endl;
	myfile << "\tStudent Year: " << te.dYear << endl << endl;
	myfile.close();
}

void Delete()
{
	tmp = front;
	if (front != NULL)
	{
		if (tmp->next != NULL)
		{
			tmp = tmp->next;
			cout << "Student ID will be delete: " << front->sId << endl
				<< "Student Name: " << front->sName << endl
				<< "Student Course: " << front->sCourse << endl
				<< "Student Year: " << front->sYear << endl;
			system("pause");
			delete front;
			front = tmp;
		}
		else
		{
			cout << "Student ID will be delete: " << front->sId << endl
				<< "Student Name: " << front->sName << endl
				<< "Student Course: " << front->sCourse << endl
				<< "Student Year: " << front->sYear << endl;
			system("pause");
			delete front;
			front = NULL;
			rear = NULL;
		}
	}
	else
	{
		cout << "The Queue is Empty" << endl;
		system("pause>0");
		return;
	}
}

void display()
{
	system("cls");
	tmp = front;
	if ((front == NULL) && (rear == NULL))
	{
		cout << "Queue is empty" << endl;
		system("pause>0");
		return;
	}
	cout << "Students: " << endl;
	while (tmp != NULL)
	{
		cout << endl << "\tStudent ID: " << tmp->sId << endl;
		cout << "\tStudent Name: " << tmp->sName << endl;
		cout << "\tStudent Course: " << tmp->sCourse << endl;
		cout << "\tStudent Year: " << tmp->sYear << endl;
		tmp = tmp->next;
	}
	system("pause>0");
}

int main()
{

	ofstream myfile;
	myfile.open("Student Data.txt", ios::out | ios::app);
	myfile << "Student Data: " << endl;
	myfile.close();

	int n;

	do {
		system("CLS");
		cout << "Student Record System  \nQueue Implementation" << endl;
		cout << "[1] Insert" << endl;
		cout << "[2] Delete" << endl;
		cout << "[3] Display" << endl;
		cout << "[4] Search" << endl;
		cout << "[0] Exit" << endl;
		cout << "Choice: ";

		cin >> n;
		if (n == 1)
			insert();
		else if (n == 2)
			Delete();
		else if (n == 3)
			display();
		else if (n == 4)
			search();
		else if (n == 0)
		{
			cout << "Thank you for using this program!\n";
			cout << "Now exiting! Goodbye!\n";
			break;
		}
		else
		{
			cout << "Invalid choice!";
			system("pause>0");
		}

	} while (1);//infinite para hindi mawala ung list
}

