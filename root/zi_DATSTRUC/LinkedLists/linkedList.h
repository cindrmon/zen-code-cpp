#include<iostream>
#include<cstdlib>

struct Node{

    int someData;
    int OtherData;
    char someChar;
    Node* nextNode;

};

void someOtherFunction() {

    Node* SamplePointer;
    SamplePointer = (Node*) malloc(sizeof(Node));

    SamplePointer->someData = 200;
    SamplePointer->OtherData = 300;
    SamplePointer->someChar = 'e';

    std::cout << SamplePointer->someData
              << std::endl << SamplePointer->OtherData
              << std::endl << SamplePointer->someChar
            ;

}
