#include<iostream>
#include<cstdlib>

using namespace std;

struct NodeTemplate{

    double someData;
    NodeTemplate* Next;

} *Genesis=NULL, *Platform, *Data;

bool LinkedListEmpty() {

    if(Genesis==NULL)
        return 1;
    else
        return 0;

}


void LinkedListInput(int ListSize) {

    for(int IDX=1; IDX<=ListSize; ++IDX){

        Data = new NodeTemplate;

        cout << "Input data on node " << IDX << ": ";
        cin >> Data->someData;
        Data->Next = NULL;

        if(LinkedListEmpty()){

            Genesis = Data;
            Platform = Data;

        }

        else{

            Platform->Next = Data;
            Platform = Data;

        }

    }

}

void LinkedListOutput() {

    int IDX;

    if(LinkedListEmpty())
        cout << "\nThe List is Empty!\n";

    else{

        Platform = Genesis;
        IDX = 1;

        while(Platform!=NULL){

            cout << "\nThe Values of Node " << IDX << " are " << Platform->someData  << ".";
            cout << endl;

            ++IDX;
            Platform = Platform->Next;

        }

    }

}

int LinkedListCount() {

    int counter=0;

    Platform = Genesis;

    while(Platform!=NULL){

        ++counter;
        Platform=Platform->Next;

    }

    return counter;

}

void LinkedListLocateItem(double SearchData) {

    int counter;
    Platform = Genesis;

    if(LinkedListEmpty())
        cout << "List is empty!\n";

    else{

        counter = 1;

        while((Platform != NULL) && (Platform->someData != SearchData)) {

            Platform = Platform->Next;
            ++counter;

        }

        if(Platform!=NULL)
            cout << "Node requested is node " << counter << ".";
        else
            cout << "Node is not found...";

    }

}

void LinkedListInsertItem(int Position, double NewData) {

    NodeTemplate *TemporaryData;

    NodeTemplate *Ante, *Post; int counter;

    // TemporaryData = new NodeTemplate;
    TemporaryData = (NodeTemplate*) malloc(sizeof(NodeTemplate));
    TemporaryData->someData = NewData;
    TemporaryData->Next = NULL;

    if(Position == 1){

        TemporaryData->Next = Genesis;
        Genesis = TemporaryData;

    }
    else if(Position > 1){

        // Ante -> Pos-1
        // Post -> Pos

        Ante = Genesis;
        Post = Genesis->Next;

        counter = 1;

        while(counter <= (Position - 2)){

            Ante = Ante->Next;
            Post = Post->Next;
            ++counter;

        }

        TemporaryData->Next = Post;
        Ante->Next = TemporaryData;

    }

}

void LinkedListDeleteItem(double  DeleteData) {

    NodeTemplate *Remove, *Ante;

    Remove = Genesis;

    if(LinkedListEmpty())

    else if(Genesis->someData == DeleteData)

        else{

            while(Remove!=NULL && Remove)

        }

    }

}

int main() {

    int ListSize;

    cout << "Enter the number of nodes: ";
    cin >> ListSize;

    LinkedListInput(ListSize);

    LinkedListOutput();

}