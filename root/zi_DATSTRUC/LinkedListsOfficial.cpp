#include<iostream>

using namespace std;

struct Node {

    int SomeData;
    Node* Next;

} *Start, *Platform, *ActualData;


int main() {

    Node *node1, *node2, *node3, *node4;
    // first node
    // creates new Node from template Node to node 1
    node1 = new Node;

    // inputting data inside node1 as a Node
    node1->SomeData = 999;

    // 
    Platform = node1;
    Start = node1;

    // second node
    node2 = new Node;

    node2->SomeData = 1000;

    // points node1 to node2 whilst Platform is still pointing in first node
    Platform->Next = node2;
    Platform = node2;

    //third node
    node3 = new Node;

    node3->SomeData = 1004;

    // points node2 to node3
    Platform->Next = node3;
    Platform = node3;

    //fourth node
    node4 = new Node;

    node4->SomeData = 9999;

    Platform->Next = node4;
    Platform = node4;

    // if node is the last node, do this
    node4->Next = NULL;

}