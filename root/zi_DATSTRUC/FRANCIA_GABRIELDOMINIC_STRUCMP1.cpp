#include <iostream>
#define NO_OF_STUDENTS 5

using namespace std;

struct studentType {

    char name[51];
    int score;
    float grade;

};

void inputStudentData(studentType students[NO_OF_STUDENTS]) {

    for(int i=0; i < 5; ++i) {

        cout << "Enter the name of student " << i+1 << ": ";
        cin.get(students[i].name,51);
        cin.get();

        cout << "Enter the test score of student " << i+1 << ": ";
        cin >> students[i].score;

        cout << "Enter the relevant grade of student " << i+1 << ": ";
        cin >> students[i].grade;

    }

}

void outputStudentData(studentType students[NO_OF_STUDENTS]) {

    for(int i=0; i<NO_OF_STUDENTS; ++i) {

        cout << "============================================" << endl;
        cout << "Student no. " << i+1 << ": " << endl;
        cout << "Name: " << students[i].name << endl;
        cout << "Test Score: " << students[i].score << endl;
        cout << "Equivalent Grade: " << students[i].grade << endl;
        cout << "============================================" << endl << endl; 

    }

}

int highestTestScore(studentType students[NO_OF_STUDENTS]) {

        //students[i].score 

    int highScore = 0;

    for(int i=0; i<NO_OF_STUDENTS; ++i) {

        if(students[i].score>highScore)
            highScore = students[i].score;

    }

    return highScore;

}

void nameofHighestTestScore(studentType students[NO_OF_STUDENTS], int highScore) {

    for(int i=0; i<NO_OF_STUDENTS; ++i) {

        if(highScore==students[i].score)
            cout << students[i].name;
            break;

    }

}

int main() {

    int highScore;
    studentType students[NO_OF_STUDENTS];

        //for(int i=0; i<NO_OF_STUDENTS; ++i)
        inputStudentData(students[NO_OF_STUDENTS]);

    system("cls");

    //for (int i = 0; i < NO_OF_STUDENTS; ++i)
        outputStudentData(students[NO_OF_STUDENTS]);

    highScore = highestTestScore(students[NO_OF_STUDENTS]);

    cout << endl << endl;

    cout << "The student with the highest test score is: ";
    //for (int i = 0; i < NO_OF_STUDENTS; ++i)
    nameofHighestTestScore(students[NO_OF_STUDENTS], highScore);
    cout << ", with the score of " << highScore << ".";
    

}