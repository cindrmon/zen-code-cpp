/*

	functions.h
	
	contains:
		
		introOutput() -> outputs the menu items
		inputData() -> outputs the input to user
		outputData() -> outputs the final total based from input from user
		
		miname jeff

*/



//basic preprogramming stuff
#include <iostream>
#include <cstring>
#include <iomanip>

//loop linit for how many orders 
#define REPETITION_SIZE 1000
#define FIX_VALUE 3

//external header data 
#include "breakfastData.h"
#include "aliases.h"

//since we use std
using namespace std;


//this is that its not necessary to like, show this so many times in deciding your order
//since its just the same menu, duh
void introOutput() {
	
	cout << "====================================" << endl;
	cout << "Welcome to NaN Restaurant!" << endl << endl;
	
	for(int i=0; i<MENU_SIZE; ++i) {
		
			cout << FIX(FIX_VALUE) << i+1 << "\t" << menu[i].foodName << menu[i].foodPrice << endl;
		
	}
	
	cout << endl << endl << "6\tFinish" << endl;
	cout << "====================================" << endl << endl;
	
}

//inputs data
void inputData() {
	
	int choice;
	while(1) { 	
	/* TODO (#1#): do infinite looping here instead of having 
	    REPETITION_SIZE */
	    
		cout << "Enter the number of your choice: ";
		cin >> choice;
		
		
		//while loop that catches if the value is less 0 and more 6
		//its supposed to be an invalid input
		Catch(choice<=0 || choice>6) {
			
			cout << "Try Again! Dips***!" << endl; //just realized the profanity here, im screwed ;-;
			cout << "Enter the number of your choice: ";
			cin >> choice;
			
		}
		
		//actual selection of the array menu[]
		if(choice<6) {
			
			++menu[choice-1].foodCounter;
			cout << endl << "You have chosen " << menu[choice-1].foodName << endl << endl;
			
		}
		
		//stops the loop once user inputs '6'
		else 
			break;
		
	}	
}

void discounts(int i) {

	double tempSubtotal = 0;

		if(menu[i].foodCounter == 20) {

			tempSubtotal = menu[i].foodPrice / (menu[i].foodPrice*0.30);
			
		}
		elif(menu[i].foodCounter > 20)
			menu[i].foodPrice = tempSubtotal;
}


//final receipt
void outputData() {
	
	//we don't want garbage data in here, right?
	double totalPrice=0.00;
	
	//i really love my equals sign
	cout << "=============================================================" << endl;
	cout << "Thank you for your choices. Your bill is: " << endl << endl;
	
	/*
		a loop accessing each individual menu[] items and checks if there is anything
		inputted to it. if none, it stops this loop and it will not output, if there are, 
		it prints the output and how many times the user has inputted the data
	*/
	for(int i=0; i<MENU_SIZE; ++i) {
		
		discounts(i);
		//skips any menu[] item that has not been inputted by the user
		if(menu[i].foodCounter == 0)
			continue;
			
		else {
			
			/*
				multiplies the number of times the user inputs the menu[] item by the price, 
				and stores it in its own double, menu[].foodSubtotal, where it stores total 
				amount for this particular menu[] item
			*/
			menu[i].foodSubtotal = menu[i].foodCounter*menu[i].foodPrice;
			
			if(menu[1].foodName || menu[4].foodName)
				cout << FIX(FIX_VALUE) << menu[i].foodCounter << " orders of " << menu[i].foodName << "\t" << menu[i].foodSubtotal << " pesos" << endl;
			else
				cout << FIX(FIX_VALUE) << menu[i].foodCounter << " orders of " << menu[i].foodName << "\t\t" << menu[i].foodSubtotal << " pesos" << endl;
				
			//adds all the subtotals to get the grand total of each
			totalPrice += menu[i].foodSubtotal;
		
		}					
	}
	

	//prints out the total amount
	cout << FIX(2) << endl << endl << "TOTAL AMOUNT:\t\t\t\t\t" << totalPrice << " \tpesos" << endl;
	cout << "=============================================================" << endl;
	
}
