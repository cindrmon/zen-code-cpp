#include <iostream>

#include "functions.h"

using namespace std;

//literally function calls only

int main() {
	
	
	// where the input all goes
	//headers used: "functions.h"
	system("cls");
	introOutput();
	inputData();
	
	//clears screen and outputs data
	system("cls");
	outputData();
	
	//prompts the user to enter any key to fully exit the program
	cout << endl << endl;
	system("pause");
	
}
