/*

	breakfastData.h
	
	contains:
		
		the data structure for the menu
		the menu itself
		2 cups white sugar
		2 1/8 cups all-purpose flour
		3/4 cup unsweetened cocoa powder
		...
		
		why am i giving you all these recipes?
		look it up yourself:
		
		https://www.allrecipes.com/recipe/8095/black-forest-cake-i/

*/

//size limit for the menu items. change if necessary
#define MENU_SIZE 5

struct menuType {
	
	char foodName[51];
	double foodPrice;
	int foodCounter;
	double foodSubtotal; 
	
} menu[MENU_SIZE] = {
		/*
			insert menu items here
			follow format:
			{"NameOfFoodItem",costOfFoodItem,0}, 
												-> the '0' means the counter from the user input, 
													initialized to 0, or something like that
		*/
		{"Continental Breakfast\t",349.99,0},
		{"Filipino Breakfast\t\t",285.00,0},
		{"Fruit Platter\t\t",172.50,0},
		{"French Toast\t\t",95.25,0},
		{"Pancakes\t\t",65.00,0}
	};
