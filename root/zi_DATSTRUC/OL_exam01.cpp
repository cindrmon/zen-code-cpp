#include<iostream>

#define MAX_SIZE 1000

typedef int ELEMENT_TYPE;
typedef int LIST_SIZE;

using namespace std;

/* 
    Important Note: If you are looping from 0, always go "less than or equal to"
                    if you are looping from n, always go "more than or equal to"
        
*/
void printReverse(ELEMENT_TYPE aList[], LIST_SIZE ListSize) {

    int IDX;


    for (IDX = ListSize - 1; IDX >= 0; --IDX)
    {

        cout << aList[IDX] << ", ";
    }

}

int countTarget(ELEMENT_TYPE aList[], LIST_SIZE ListSize, int target) {

    int IDX=0, targetLocation;

        while( (IDX!=ListSize) ) {

            if(aList[IDX]==target){

                targetLocation=IDX;
                return targetLocation+1;

            }

            ++IDX;

        }

        

    return -1;

}

void insertItem(ELEMENT_TYPE aList[], LIST_SIZE *ListSize, int NewData, LIST_SIZE Pos) {

    int IDX, Tmp=0;

        if(ListSize<0)
            cout << "\nThe List is empty!";

        else{

            for(IDX=(*ListSize); IDX >= Pos; --IDX )
                aList[IDX+1] = aList[IDX];
            
            aList[Pos] = NewData;

            ++(*ListSize);

        }

        cout << "The current list is now: " << endl;

        for(IDX=0; IDX<*ListSize; ++IDX) {

            cout << aList[IDX] << " @ " << IDX+1 << endl;

        }
        cout << endl << "With " << *ListSize << " items." << endl;
}

void printPrimeNosAndPos(ELEMENT_TYPE aList[], LIST_SIZE ListSize) {

    int IDX, IIDX, PrimeSum;
    bool isPrime;

    if(ListSize<0)
        cout << "\nThe List is Empty!";

    else{

        for(IDX=0; IDX < ListSize; ++IDX){

            PrimeSum=0;

            for(IIDX=1; IIDX<= aList[IDX]; ++IIDX){

                if((aList[IDX]%IIDX)==0)
                    ++PrimeSum;

            }

            if(PrimeSum==2)
                cout << endl << aList[IDX] 
                     << " is a prime number, located at "
                     << "position " << IDX+1 << endl;

        }

    }

}

int main() {

    ELEMENT_TYPE list[MAX_SIZE]={
        8, 10, 9, 67,83, 7, 44, 3, 23
    }, newData;
    LIST_SIZE listSize=9, pos;
    int targetValue, targetLocation;

    cout << "Welcome to the exam program!\n"
         << "The first output would be the list itself printed backwards: ";

    printReverse(list, listSize);

    cout << "\nPretty epic eh? Moving on, please input which value\n"
         << "you wanna see in this set: ";

    cin >> targetValue;

    targetLocation=countTarget(list, listSize, targetValue);

    if(targetLocation==-1) 
        cout << "Can't find it, sorry, better luck next time\n\n";

    else 
        cout << "Target is found at position " << targetLocation << ". Pretty cool eh?\n\n";

    cout << "Now we wanna add something to our crummy list.\n"
         << "Enter the position you wanna put it (must be less than 9): ";

    cin >> pos;

    cout << "Great! now what integer data you wanna put inside?: ";
    cin >> newData;

    insertItem(list, &listSize, newData, pos);

    cout << "\nIf you've reached this far, congratulations! the program is working!\n\n"
         << "Now to print the masterpiece (that is, it only prints prime numbers and their\n"
         << "location): ";

    printPrimeNosAndPos(list, listSize);

    cout << "if you see this message with all the proper output, congrats! you get a \n"
         << "perfect score!\n";

}

