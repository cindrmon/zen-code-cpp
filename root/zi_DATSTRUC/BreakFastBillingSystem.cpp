#include <iostream>
#include <cstring>

#define MENU_SIZE 5

#define Try do
#define Catch while

using namespace std;

struct menuType {

    char food[26];
    float price;
    int count;

} 
    menu[MENU_SIZE] = {
        {"Continental Breakfast",349.99},
        {"Filipino Breakfast",285.00},
        {"Fruit Platter",172.50},
        {"French Toast",95.25},
        {"Pancakes",65.00}

    }, menuChoices[MENU_SIZE];

/*void intro() {

    cout << endl;
    cout << "=============================================================";
    cout << "Welcome to NaN Restaurant! " <<endl << endl;

    for(int i=0; i<MENU_SIZE; ++i) {

        cout << i+1 << "/t" << 

    }
}
*/

float inputData() {

    int choice;
    float total=0.0;

    for(int i=0; i<MENU_SIZE; ++i) {


        cout << "Enter the number of your choice: ";
        cin >> choice;

        Catch(choice > 6 || choice <= 0){
            cout << "Invalid Choice! " << endl;
            cout << "Enter the number of your choice: ";
            cin >> choice;
        };

        switch(choice) {

            case 1:
                menuChoices[i].price = menu[0].price;
                strcpy(menuChoices[i].food,menu[0].food);
                menuChoices[i].count += menu[0].count;
                cout << "You have chosen " << menu[0].food << endl;
                total += menu[0].price;
                break;

            case 2:
                menuChoices[i].price = menu[1].price;
                strcpy(menuChoices[i].food, menu[1].food);
                total += menu[1].price;
                menuChoices[i].count += menu[1].count;
                cout << "You have chosen " << menu[1].food << endl;
                break;

            case 3:
                menuChoices[i].price = menu[2].price;
                strcpy(menuChoices[i].food, menu[2].food);
                total += menu[2].price;
                menuChoices[i].count += menu[2].count;
                cout << "You have chosen " << menu[2].food << endl;
                break;

            case 4:
                menuChoices[i].price = menu[3].price;
                strcpy(menuChoices[i].food, menu[3].food);
                total += menu[3].price;
                menuChoices[i].count += menu[3].count;
                cout << "You have chosen " << menu[3].food << endl;
                break;

            case 5:
                menuChoices[i].price = menu[4].price;
                strcpy(menuChoices[i].food, menu[4].food);
                total += menu[4].price;
                menuChoices[i].count += menu[4].count;
                cout << "You have chosen " << menu[4].food << endl;
                break;

            case 6:
                return total;

            default:
                cout << "You have selected an invalid choice, try again";
        }

    } 

    return total;
}

void outputData(float total) {

    cout << endl << endl;

    cout << "=============================================================";
    cout << "Thank you for your choice!Bill is: " << endl;
    cout << endl;

    for(int i=0; i<MENU_SIZE; ++i) {

        cout << menuChoices[i].count << "\t" << menuChoices[i].food << "\t\t" << menuChoices[i].price << endl;

    }

    cout << "\t\t\t\t\t\t\t" << total;


}

int main() {

    
    float total;

    total = inputData();
    outputData(total);

}

