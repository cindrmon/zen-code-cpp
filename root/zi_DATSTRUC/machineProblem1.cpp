#include <iostream>
#include "listOperators.h"

#define cout std::cout
#define cin std::cin

typedef int ChoiceType;

//================================================================================================================
//================================================================================================================
// MAIN MENU FUNCTIONS
void menu(ARRAY_SIZE listSize) {

    cout << "====================================\n";
    cout << "LIST (ARRAY) PROGRAM\n";
    cout << "====================================\n";
    cout << "There are currently " << listCountItem(listSize) << " items in the list.\n";

    cout << endl << endl;
    cout << "====================================\n";
    cout << "Options:\n";
    cout << "(1). Add item to the list\n"
         << "(2). Delete Item From The list\n"
         << "(3). Locate Items in the list\n"
         << "(4). Print Items in the list\n"
         << "(5). Exit Program\n"
         << "====================================\n";
    
    cout << endl << endl;

    cout << "Enter the number of your choice";

}

void menuInvalid() {

    cout << "Invalid choice!\n";
    cout << "Enter the number of your choice";

}

void selector(ChoiceType *choice){

    cout << "\n>> ";
    cin >> *choice;

}

void exitProgram() {
    cout << "\nThank you for using this program!";
}
//================================================================================================================
//================================================================================================================





//================================================================================================================
//================================================================================================================
// Choice Functions
void choiceOne_AddItem (ARRAY aList[], USER_INPUT newData, ETC newDataPos, ARRAY_SIZE *listSize) {

    if (listEmpty(*(listSize)) == 1) {

        cout << "List is empty! Will automatically insert at slot 0!\n";

        cout << "Please enter data\n" << ">> ";
        cin >> newData;

        listAddItem(aList, newData, 0, listSize);

    }

    else {

        cout << "List is not empty! Which slot would you want to place it?\n"
             << "NOTE: Must not be less than " << *(listSize) << ", also not a negative number\n";

        cout << "\n>> ";
        cin >> newDataPos;

        while( ( newDataPos>*(listSize) ) || (newDataPos<=1) ) {

            cout << "Invalid input! Please try again...\n"
                 << "Which slot would you want to place it?\n"
                 << "NOTE: MUST NOT BE LESS THAN " << *(listSize) << ", ALSO NOT A NEGATIVE NUMBER\n";

            cout << "\n>> ";
            cin >> newDataPos;
        }

        cout << "Please enter data at slot " << newDataPos << endl 
             << ">> ";
        cin >> newData;

        listAddItem(aList, newData, newDataPos, listSize);

    }
}


void ChoiceTwo_RemoveItem(ARRAY aList[], ETC rmDataPos,  ARRAY_SIZE *ListSize){

    if( listEmpty( *(ListSize)) == 1 ) {

        listDeleteItem(aList, rmDataPos, ListSize);

    }
    else {

        cout << "Please enter which slot to delete\n"
             << "NOTE: Must not be less than " << *ListSize << ", also not a negative number\n";

        cout << "\n>> ";
        cin >> rmDataPos;

        while (rmDataPos>*(ListSize) || rmDataPos <= 1 ) {

            cout << "Invalid input!\n"
                 << "Please enter which slot to delete\n"
                 << "NOTE: MUST NOT BE LESS THAN " << *(ListSize) << ", ALSO NOT A NEGATIVE NUMBER\n"
                 << "\n>> ";
            cin >> rmDataPos;

        }  

        listDeleteItem(aList, rmDataPos, ListSize);      

    }
}

void ChoiceThree_SearchItem(ARRAY aList[], USER_INPUT searchValue, ARRAY_SIZE *ListSize) {

    cout << "\nEnter the data to search";
    cout << "\n>> ";
    cin >> searchValue;

    listLocateItem(aList, searchValue, *ListSize);

}

void ChoiceFour_PrintItems(ARRAY aList[], ARRAY_SIZE ListSize) {

    listPrintItems(aList, ListSize);

}
//================================================================================================================
//================================================================================================================

int main() {

    ARRAY_SIZE listSize=5;
    ARRAY listSample[listSize]={0};
    ChoiceType choice;
    USER_INPUT newData, searchValue;
    ETC newDataPos, rmDataPos;    
            
    menu(listSize);

    while(1) {

        selector(&choice);

        if( (choice>5)||(choice<0) ) {

            menuInvalid();
            selector(&choice);

        }

        else if(choice==5) {

            exitProgram();

        }

        else {

            switch(choice) {

                case 1:
                    choiceOne_AddItem(listSample, newData, newDataPos, &listSize);
                    cout << "\n\n";
                    break;
                
                case 2:
                    ChoiceTwo_RemoveItem(listSample, rmDataPos, &listSize);
                    cout << "\n\n";
                    break;

                case 3:
                    ChoiceThree_SearchItem(listSample, searchValue, &listSize);
                    cout << "\n\n";
                    break;
                
                case 4:
                    ChoiceFour_PrintItems(listSample, listSize);
                    cout << "\n\n";
                    break;

                default:
                    exitProgram();

            }
        }

    }

}