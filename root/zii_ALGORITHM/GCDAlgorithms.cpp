#include<iostream>
#include<bits/stdc++.h>

using namespace std;

int EuclidsGCD(int m, int n) {

    int rem;

    while(n != 0){

        rem = m % n;
        m = n;
        n = rem;

    }

    return m;
}

int BruteForceGCD(int m, int n){

    int t;

    // min(m, n)
    if(m > n)
        t = n;
    else
        t = m;

    while(t > 0){
        
        if(m % t == 0 && n % t == 0)
            break;
        --t;
    }

    return t;

}

int* SieveOfEratosthenes(int num){

    int idx, jdx, numSize;

    int listOfNumbers[num+1];
    static int siftedNumbers[1300];
    // single dimensional array starting from 2 to num
    for(idx = 0; idx <= num; ++idx){
        listOfNumbers[idx] = idx;
    }
    
    // X ITERATION; let p = n; start at p^2
    // start eliminating all numbers that are multiples of n starting at p^2

    // Stop iterating when p^2 > num or p > sqrt(num)
    for(idx = 2; idx <= sqrt(num); ++idx) {
        if(listOfNumbers[idx] != 0) {
            jdx = idx * idx;

            while(jdx <= num) {
                listOfNumbers[jdx] = 0;
                jdx += idx;
            }
        }
    }

    // transfer prime numbers to a fresh array
    jdx = 0;
    for(idx = 2; idx <= num; ++idx) {
        if(listOfNumbers[idx] != 0) {
            siftedNumbers[jdx] = listOfNumbers[idx];
            ++jdx;
        }
    }

    return siftedNumbers;

}

int PrimeFactorizationGCD(int m, int n){
    // find the prime numbers of m
    // find the prime numbers of n
    // identify common factors in the two prime expansions; repeated min(p_m, p_n) times
    return 0;
}

int main() {

    int num1, num2, num3, eucGCD, brtGCD;
    int* ArrayOfPrimeNumbers;

    time_t start, end;

    double time_taken;

    num1 = 80;
    num2 = 60;
    num3 = 100;


    // time(&start);
    // ios_base::sync_with_stdio(false);
    // eucGCD = EuclidsGCD(num1, num2);
    // time(&end);

    // time_taken = double(end - start);

    // cout << "--- EUCLID'S GCD ---" << endl
    //     << "Variables Used:\n"
    //     << "m = " << num1 << endl
    //     << "n = " << num2 << endl
    //     << "GCD(" << num1 << ", " << num2 << ") = " << eucGCD << endl
    //     << endl << "Time Taken: \n"
    //     << fixed << time_taken << setprecision(3)
    //     << " seconds.\n"
    //     << "---------------------";

    // cout << endl << endl;

    // time(&start);
    // ios_base::sync_with_stdio(false);
    // brtGCD = BruteForceGCD(num1, num2);
    // time(&end);

    // time_taken = double(end - start);

    // cout << "--- BRUTE FORCE GCD ---" << endl
    //     << "Variables Used:\n"
    //     << "m = " << num1 << endl
    //     << "n = " << num2 << endl
    //     << "GCD(" << num1 << ", " << num2 << ") = " << brtGCD << endl
    //     << endl << "Time Taken: \n"
    //     << fixed << time_taken << setprecision(3)
    //     << " seconds.\n"
    //     << "------------------------";

    ArrayOfPrimeNumbers = SieveOfEratosthenes(num3);

    for(int idx = 0; idx < 30; ++idx) {
        cout << ArrayOfPrimeNumbers[idx] << ", ";
    }

}