/*

    THIOJOE COMPUTER QUIZ!

    Questions                                Choices                 Equivalent Points

    1. What OS are you using?               Windows                  1
                                            Linux                    5
                                            Mac                      (-2)
                                            I don't Know             (-999)

    2. Do you know what version of OS       boolean (yes/no)         yes == 1 
        are you using?                                               no == 0

    3. Are you someone who asks for         boolean (yes/no)        yes == -1
        computer help?                                              no == 0

    4. Are you someone who gives computer   boolean (yes/no)        yes == 1
        help?                                                       no == 0

    5. Are you a hunt-and-peck typer?       boolean (yes/no)        yes == -5
                                                                    no == 0
    
    6. Do you know how big your hard        boolean (yes/no)        yes == 1 
        drive is in GB/GiB?                                         no == 0

    7. Do you know your current screen      boolean (yes/no)        yes == 1
        resolution?                                                 no == 0

    8. Do you know how many cores your      boolean (yes/no)
        CPU has?                            if(yes)             
                                                cin >> no.ofCores   no.ofCores / 2

                                            else                    (-1)

    9. Do you use your computer's           boolean (yes/no)        yes == (-1)
        default browser?                                            noo == 0

    10. Have you ever opened up your        boolean (yes/no)        yes == 2
        computer case?                                              no == 0

    11. Have you ever used linux?           boolean (yes/no)        yes == 2
                                                                    no == 0
                                                
    12. Do you back up your hard drive?     boolean (yes/no)        yes == 2 
                                                                    no == (-3)

    13. Do you know whether your computer   boolean (yes/no)        yes == 1
        is Intel or AMD?                                            no == 0

    14. Have you ever changed your          boolean (yes/no)        yes == 2
        browser homepage?                                           no == (-1)

    15. How many hours do you spend on      cin >> no.ofHours       no.ofHours
        your computer daily, not counting
        during class or work?

    16. Have you ever used command prompt?  boolean (yes/no)        yes == 2
                                                                    no == 0
                                                                
    17. Do you know the difference          boolean (yes/no)        yes == 1
        between Java and Javascript?                                no == 0

    18. Do your passwords contain special   boolean (yes/no)        yes == 1
        characters?                                                 no == 0

    19. Have you ever installed new         boolean (yes/no)        yes == 3
        hardware in your computer?                                  no == 0

    20. How many extra monitors do you      cin >> no.ofEMonitors   no.ofEMonitors
        have?

    21. Do you know the difference          boolean (yes/no)        yes == 1
        between HTTP and HTTPS?                                     no == 0

    22. Have you ever built a computer?     boolean (yes/no)        yes == 4
                                                                    no == 0

    23. Have you overclocked your CPU       boolean (yes/no)        yes == 5
        manually?                                                   no == 0

    24. Do you know how main memory timings boolean (yes/no)        yes == 3
        are and how do they work?                                   no == 0

    25. People you may know in the          boolean (yes/no)        for every yes == 1
        computer world:                     (per person)            for every no == 0
        
        a. Steve Wozniak
        b. Dennis Ritchie
        c. Richard Stallman
        d. Grace Hopper
        e. Kevin Mitnick
        f. Linus Torvalds
        g. John Carmack

    26. Do you know what the Insert Key     boolean (yes/no)        yes == 1
        does?                                                       no == 0

    27. Do you know what the Scroll Lock    boolean (yes/no)        yes == 3
        key does?                                                   no == 0

    28. Have you not have a computer        boolean (yes/no)        yes == 3
        virus in the past 6 months?                                 no == 0

    29. Do you need an antivirus?           boolean (yes/no)        yes == 0
                                                                    no == (-5)

    30. Do you encrypt your hard drive?     boolean (yes/no)        yes == 4
                                                                    no == 0

    31. Do you have a home server?          boolean (yes/no)        yes == 5
                                                                    no == 0

    32. Do you know waht a network          boolean (yes/no)
        switch is?                          if(yes)
                                                goto 33.
                                            else                    (-1)
                                        
    33. How many network switches do        cin >> no.ofNetSwitches no.ofNetSwitches
        you have?                           

    34. Have you ever edited the windows    boolean (yes/no)        yes == 5
        registry?                                                   no == 0

    35. Can you writed a "hello world"      boolean (yes/no)        yes == 5
        program with any programming                                no == 0
        language?                           

    36. Have you ever spent more than       boolean (yes/no)        yes == 5
        1/3rd of your monthly income/       if(no)
        allowance on a computer part?           goto 37.

    37. Have you asked for your parents     boolean (yes/no)        yes == (-10)
        for a computer part?                                        no == 0

    38. Do you know what Google Ultron      boolean (yes/no)        yes == 3
        is?                                                         no == 0

    39. Besides your default browser,       cin >> no.ofBrowsers    no.ofBrowsers
        how many more browsers have you     
        installed in your pc?               

    40. Have you ever made your own         boolean (yes/no)        yes == 8
        ethernet cable?                                             no == 0

    41. Have you ever installed custom      boolean (yes/no)        yes == 10
        firmware on your router?                                    no == 0

    42. Have you ever resized a drive       boolean (yes/no)        yes == 10
        partition?                                                  no == 0

    43. Have you ever used SSH to rem-      boolean (yes/no)        yes == 10
        otely access a pc?                                          no == 0

    44. Do you use custom DNS servers?      boolean (yes/no)        yes == 10
                                                                    no == 0

    void ConditionalScoring (score) { 

        if(score<=0)
            cout << "F-"
        else if(score<20)
            cout << "Computer Illiterate"
        else if(score<50)
            cout << "Average Computer Literate"
        else if(score<110)
            cout << "Above Average Computer Literate"
        else if(score<140)
            cout << "Non-Professional Computer Expert"
        else
            cout << "Pure Computer Expert/Computer God"

    }

*/

/*

    OUTPUT: (every cls means screen must be cleared)

    cls

    ==================================
    WELCOME TO THIOJOE COMPUTER QUIZ!
    ==================================

    (per question, screen MUST be clear. you can use `system("cls")` to clear console screen in C++ windows)

    EXTRA NOTE: only type until the range of the '=======' print for consistency

    TOTAL SCORE: <score>
    =====================================================
    Q1. What OS are you using? Type one of the choices:
        - Windows
        - Mac
        - Linux
        - idk

    Type Here: <user_input>
    =====================================================

    cls 

    TOTAL SCORE: <score>
    =====================================================
    Q2. Do you know what OS version are you using? 
    (yes/no)

    Type Here: <user_input>
    ===================================================== 

    cls

    TOTAL SCORE: <score>
    =====================================================
    Q3. Are you someone who asks for help? (yes/no)

    Type Here: <user_input>
    ===================================================== 

    cls

    ...

    (after the last question has been inputted, this must be the next one to appear:)

    ======================================
    FINAL SCORE: <score>

    You are...

    ConditionalScoring(score)

    THANK YOU FOR TRYING THIS QUIZ!!!
    ======================================

    system("pause") --> put this as the last line of the entire program

*/