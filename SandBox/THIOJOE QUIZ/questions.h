#include <iostream>

using namespace std;

void readQuestion(int questionNo) {

    char Questions[][140] = {

        /*0*/ "1. What OS are you using? ",
        /*1*/ "2. Do you know what version of OS are you using? ",
        /*2*/ "3. Are you someone who asks for computer help? ",
        /*3*/ "4. Are you someone who gives computer help? ",
        /*4*/ "5. Are you a hunt-and-peck typer? ",
        /*5*/ "6. Do you know how big your hard drive is in GB/GiB? ",
        /*6*/ "7. Do you know your current screen resolution? ",
        /*7*/ "8. Do you know how many cores your CPU has? ",
        /*8*/ "9. Do you use your computer's default browser? ",
        /*9*/ "10. Have you ever opened up your computer case? ",
        /*10*/ "11. Have you ever used linux? ",
        /*11*/ "12. Do you back up your hard drive? ",
        /*12*/ "13. Do you know whether your computer is Intel or AMD? ",
        /*13*/ "14. Have you ever changed your browser homepage? ",
        /*14*/ "15. How many hours do you spend on your computer daily, not counting during class or work? ",
        /*15*/ "16. Have you ever used a command prompt? ",
        /*16*/ "17. Do you know the difference betwrrn Java and Javascript? ",
        /*17*/ "18. Do your passwords contain special characters? ",
        /*18*/ "19. Have you ever installed new hardware in your computer? ",
        /*19*/ "20. How many extra monitors do you have? ",
        /*20*/ "21. Do you know the difference between HTTP and HTTPS? ",
        /*21*/ "22. Have you ever built a computer? ",
        /*22*/ "23. Have you overclocked your CPU manually? ",
        /*23*/ "24. Do you know how main memory timings are and how do they work? ",
        /*24*/ "25. People you may know in the computer world:\n\ta. Steve Wozniak ",
        /*25*/ "25. People you may know in the computer world:\n\tb. Dennis Ritchie ",
        /*26*/ "25. People you may know in the computer world:\n\tc. Richard Stallman ",
        /*27*/ "25. People you may know in the computer world:\n\td. Grace Hopper ",
        /*28*/ "25. People you may know in the computer world:\n\te. Kevin Mitnick ",
        /*29*/ "25. People you may know in the computer world:\n\tf. Linus Torvalds ",
        /*30*/ "25. People you may know in the computer world:\n\tg. John Carmack ",
        /*31*/ "26. Do you know what the insert key does? ",
        /*32*/ "27. Do you know what the scroll lock key does? ",
        /*33*/ "28. Have you not have a computer virus in the past 6 months? ",
        /*34*/ "29. Do you need an antivirus? ",
        /*35*/ "30. Do you encrypt your hard drive? ",
        /*36*/ "31. Do you have a home server? ",
        /*37*/ "32. Do you know what a network switch is? ",
        /*38*/ "33. How many network switches do you have? ",
        /*39*/ "34. Have you ever edited the windows registry? ",
        /*40*/ "35. Can you write a 'hello world' program with any programming language? ",
        /*41*/ "36. Have you ever spend more than 1/3rd of your monthly income/allowance on a computer part? ",
        /*42*/ "37. Have you asked for your parents for a computer part? ",
        /*43*/ "38. Do you know what google Ultron is? ",
        /*44*/ "39. Besides your default browser, how many more browsers have you installed in your pc? ",
        /*45*/ "40. Have you ever made your own ethernet cable? ",
        /*46*/ "41. Have you ever installed custom firmware on your router? ",
        /*47*/ "42. Have you ever resized a drive partition? ",
        /*48*/ "43. Have you ever used SSH to remotely access a pc? ",
        /*49*/ "44. Do you use custom DNS servers? "

    };

    cout << Questions[questionNo][140];

}