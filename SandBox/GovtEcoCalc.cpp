#include <iostream>

using namespace std;

int main()
{
    int C, G, I, X, M, NFIA, DA, IT, GNP, GDE, GDP, NNP, NI; /* Grabe, andami!!!!!*/

    cout << "Enter The ff. Values\n\n";

    cout << "Personal Consumption Expenditures = ";
    cin >> C;

    cout << "Government Expenditures on Goods and Services = ";
    cin >> G;

    cout << "Capital Formation = ";
    cin >> I;

    cout << "Exports = ";
    cin >> X;

    cout << "Imports = ";
    cin >> M;

    cout << "Depreciation Allowance = ";
    cin >> DA;

    cout << "NFIA = ";
    cin >> NFIA;

    cout << "Indirect Tax = ";
    cin >> IT;

    cout << "\n\nThe Gross Domestic Expenditures or GDE is " << C + G + I << ".";
    cout << "\nThe Gross Domestic Product or GDP is " << GDE + ( X - M ) << ".";
    cout << "\nThe Gross National Product or GNP is " << GDP + NFIA << ".";
    cout << "\nThe Net National Product or NNP is " << GNP + DA << ".";
    cout << "\nThe National Income or NI is " << NNP + IT << ".";

    return 0;
}
