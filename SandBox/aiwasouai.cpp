#include <exception>
#include <iostream>
#include <new>
#include <stdexcept>
#include <string>

using namespace std;

class ZodiacAnimal {

    private:
        string AnimalName;
        int InitialYear;

    public:
        // Constructors and Destructors
        ZodiacAnimal() {
            AnimalName = "nul";
            InitialYear = 0000;

        }
        ~ZodiacAnimal(){}

        // Setters and Getters
        void setAnimalName(string AnimalName) {
            this->AnimalName = AnimalName;
        }

        void setInitialYear(int InitialYear) {
            this->InitialYear = InitialYear;
        }

        string getAnimalName() {
            return AnimalName;
        }

        int getInitialYear() {
            return InitialYear;
        }

        // Functions
        void ReturnYears(int NumberOfYears, int CurrentYear) {

            int IDX = 0;
            CurrentYear += (12 * NumberOfYears) - 13;

            while (this->InitialYear <= CurrentYear) {
                
                cout << this->InitialYear << ", ";
                ++IDX;
                this->InitialYear += 12;

            }

        }


};

int main() {

    const int NO_OF_YEARS = 5;
    const int current_year = 1996;
    int year_ctr = current_year;

    ZodiacAnimal Animal[12];
    const string ZodiacAnimals[12] = {"Rat", "Ox", "Tiger", "Rabbit", "Dragon", "Snake", "Horse", "Sheep", "Monkey", "Rooster", "Dog", "Pig"};

    int year = current_year;


    for (int idx=0; idx < 12; ++idx) {

        Animal[idx].setAnimalName(ZodiacAnimals[idx]);
        Animal[idx].setInitialYear(year_ctr);
        
        ++year_ctr;

    }

    for(int idx=0; idx < 12; ++idx){

        cout << Animal[idx].getAnimalName() << endl;
        Animal[idx].ReturnYears(NO_OF_YEARS, current_year);
        cout << endl << endl;

    }

    int counter = 0;

    while(Animal[counter].getAnimalName() != "penguin"){

        cout << year << " is the year of linux!\n";
        ++year;
        ++counter;

        if(counter >= 12)
            counter %= 12;        

    }

}